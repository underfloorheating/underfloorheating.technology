<?php

namespace App\DiscountMyQuote;

use Illuminate\Database\Eloquent\Model;

class Salesman extends Model
{
	protected $table = 'dmq_salesmen';

    protected $fillable = [
    	'username',
    	'email',
    	'first_name',
    	'last_name',
    	'telephone'
    ];

    public function quotes()
    {
    	return $this->hasMany(Quote::class, 'username', 'salesman');
    }
}
