<?php

namespace App\DiscountMyQuote;

use Illuminate\Database\Eloquent\Model;

use DB;

class Quote extends Model
{
	const SPREADSHEET_ID = '1kCUe3Od6Or7tjIkmelxri6veogfmZG5sO5L-m8_lwHQ';

	protected $table = 'dmq_quotes';

	protected $fillable = [
		'salesman_username',
		'order_number',
		'customer_order_ref',
		'order_date',
		'order_net',
		'name',
		'email',
		'discount_applied',
		'available_discount'
	];

	public function salesman()
	{
		return $this->belongsTo(Salesman::class, 'salesman_username', 'username');
	}

	/**
	 * Downloads all data from the Google Sheet and inserts it into the local DB table
	 *
	 * @return boolean
	 */
	public static function saveDataFromGoogleSheet()
	{
		$memoryLimit = ini_get('memory_limit');
		ini_set('memory_limit', '500M');

    	DB::table('dmq_quotes')->truncate();
		$service = app()->make('google_spreadsheets_client');

		$columnTitles = [
			'salesman_username',
			'order_number',
			'customer_order_ref',
			'order_date',
			'order_net',
			'name',
			'email',
			'discount_applied',
			'available_discount'
		];
    	// Pull data in from google sheet and save it locally
		$range = 'A3:I';
		$rows = $service->spreadsheets_values->get(self::SPREADSHEET_ID, $range, ['majorDimension' => 'ROWS']);

		if (!isset($rows['values'])) {
			throw new \Exception('There are no values in the spreadsheet');
		}

		foreach ($rows['values'] as $row) {
			if (empty($row[0])) {
				break;
			}
			$labelledRow = array_combine($columnTitles, $row);
			$labelledRow['order_date'] = self::strToDate($labelledRow['order_date']);
			$labelledRow['order_net'] = preg_replace('/[£,]*/', '', $labelledRow['order_net']);
			$data[] = $labelledRow;
		}
		DB::table('dmq_quotes')->insert($data);

		ini_set('memory_limit', $memoryLimit);

		return true;
	}

	/**
	 * Convert string to date
	 *
	 * @param  string $dateString The date string we want to convert
	 * @return string             The string formatted into a correct date format
	 */
	private static function strToDate($dateString)
	{
		list($day, $month, $year) = explode('/', $dateString);
		return date('Y-m-d', mktime(0, 0, 0, (int) $month, (int) $day, (int) $year));
	}
}