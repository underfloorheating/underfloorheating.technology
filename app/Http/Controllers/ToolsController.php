<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ServerException;

use \Ekomi\Api as EkomiApi;
use \Ekomi\Request\GetFeedback;
use \Ekomi\Request\GetProductFeedback;
use \Ekomi\Request\GetVisitorFeedback;

use League\Csv\Writer;

ini_set('max_execution_time', 300);

class ToolsController extends Controller
{


    public function downloadPolypipeInstallers()
    {
    	$postcodes = config('tools.districts');

		$client = new Client();

		$re = [
			'/<script[\s|\S]*?<\/script>/m',
			'/<svg[\s|\S]*?<\/svg>/m',
			'/<!--[\s|\S]*?-->/m',
			'/&.*?;/m'
		];

		$errors = [];

		$fp = fopen(storage_path().'/installers.txt', 'a');

    	foreach($postcodes as $postcode) {
    		try {
				$response = $client->request('POST', 'https://www.polypipeufh.com/where-to-buy/find-a-stockist', [
					'headers' => [
						'Accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3',
						'Accept-Encoding' =>  'gzip, deflate, br',
						'Accept-Language' =>  'en-GB,en;q=0.9',
						'Cache-Control' =>  'max-age=0',
						'Connection' =>  'keep-alive',
						'Content-Length' =>  '60',
						'Content-Type' =>  'application/x-www-form-urlencoded',
						'Cookie' =>  '_ga=GA1.2.889802901.1557237008; _gid=GA1.2.778252243.1557237008; _fbp=fb.1.1557237008749.212230832; _gac_UA-27714215-2=1.1557237045.CjwKCAjw2cTmBRAVEiwA8YMgzbDTe1YJDhk56-oYWRiSYdWXIWxEntBO4RzsixZCd18rAeSM0B8CbhoCbSwQAvD_BwE; XSRF-TOKEN=eyJpdiI6IkRJVEoxXC9kc3pEMUhMcTlVbnV1T0F3PT0iLCJ2YWx1ZSI6IkI1a3p5VkprU0ROcGVjMHFPSkVyV0lmM2wxcWdUOUpaZ1FVUE5nSFFDQ1F6T1E3ODN2dm1PVlJiM2x5NG53MHZVK04rQTE1eDVaRzhxV3kyNkdRc1J3PT0iLCJtYWMiOiIwODY3ZDc1NDRmNDc4MDFkOTIzYWYwMGE1ZGQ0NTQxZGM4MDdjZDk2MDdkNDAzYjVjMzk5YWJkODEwNmY5OWE1In0%3D; polypipeufh_session=eyJpdiI6ImxZUVZZY21JRmROK2tsMHhXTmJKT0E9PSIsInZhbHVlIjoic2hzU3VLcEQyNFwvbGU4OGFpN3hDdkx3UG4rOW9tbFRaT1o4SmViRjBPUWhMVWk3dVhNYThHOGtsSUw0ZlRrZW1EbjhvK1VrUjI3SGprREdmKzVMMVlnPT0iLCJtYWMiOiI1MTU5NWMyZGJkOTEwZjcyMjM0NWIyZDQ3MDc0NGEzODRhNmYwOTNlYjBlNzRiYTM4YjM1N2MyZWQzN2E1YjU0In0%3D; _gat_UA-27714215-2=1',
						'Host' =>  'www.polypipeufh.com',
						'Origin' =>  'https://www.polypipeufh.com',
						'Referer' =>  'https://www.polypipeufh.com/where-to-buy/find-a-stockist',
						'Upgrade-Insecure-Requests' =>  '1',
						'User-Agent' =>  'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36'
					],
					'form_params' => [
						'_token' => 'CKrICtaTtOAJPg9YsZOxqr193hY1xojgVhgLuZwb',
						'postcode' => $postcode
					]
				]);

				$body = preg_replace($re, '', (string) $response->getBody());

				$dom = new \DOMDocument( "1.0");
				$dom->formatOutput = true;
				$dom->loadHTML($body, LIBXML_NOERROR);
				$xpath = new \DOMXPath($dom);
				$elements = $xpath->query("//div[@class='form-results-group']");
				foreach($elements as $elem) {
					foreach($elem->childNodes as $child) {
						if($child->nodeType != XML_TEXT_NODE) {
							//$data[] = explode("\n", preg_replace('/\n\s*/m', "\n", trim($child->textContent)));
							fwrite($fp, $postcode."\t".preg_replace('/\n\s*/m', "\t", trim($child->textContent))."\n");
						}
					}
				}
			} catch (ServerException $e) {
				$errors[] = $e->getMessage();
				continue;
			}
		}
		fclose($fp);

    	return '';
    }

    private function getEkomiApi()
    {
    	return new EkomiApi(env('EKOMI_ID'),env('EKOMI_KEY'));
    }

    public function getEkomiReviews()
    {
		$api = $this->getEkomiApi();

		// generate a new link
		$apiCall = new GetFeedback();
		$apiCall->setRange('10y');

		$result = $api->exec($apiCall);

		$writer = Writer::createFromPath(storage_path() . '/app/ekomi-reviews.csv', 'w+');
		$writer->insertOne(['submitted', 'order_id', 'rating', 'review', 'comment']);
		foreach($result as $review) {
			$review = (array) $review;
			$review['submitted'] = date('Y-m-d', $review['submitted']);
			$writer->insertOne($review);
		}
    }

    public function getEkomiProductReviews()
    {
		$api = $this->getEkomiApi();

		// generate a new link
		$apiCall = new GetProductFeedback();
		$apiCall->setRange('10y');

		$result = $api->exec($apiCall);

		$writer = Writer::createFromPath(storage_path() . '/app/ekomi-product-reviews.csv', 'w+');
		$writer->insertOne(['submitted', 'order_id', 'product_id', 'rating', 'review']);
		foreach($result as $review) {
			$review = (array) $review;
			$review['submitted'] = date('Y-m-d', $review['submitted']);
			$writer->insertOne($review);
		}
    }

    public function getEkomiVisitorReviews()
    {
		$api = $this->getEkomiApi();

		// generate a new link
		$apiCall = new GetVisitorFeedback();

		$result = $api->exec($apiCall);

		$writer = Writer::createFromPath(storage_path() . '/app/ekomi-visitor-reviews.csv', 'w+');
		$writer->insertOne(['submitted', 'order_id', 'product_id', 'rating', 'review']);
		foreach($result as $review) {
			$writer->insertOne((array) $review);
		}
    }
}