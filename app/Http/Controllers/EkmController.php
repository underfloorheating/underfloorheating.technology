<?php

namespace App\Http\Controllers;

use App\Ekm\EkmOrder;
use App\Ekm\EkmXmlBuilder;
use App\Ekm\Sites\SiteFactory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class EkmController extends Controller
{
	public function getOrder($siteName, $id)
	{
		session(['api_application' => 'orderwise']);

		try{
	    	$site = SiteFactory::create($siteName);
	    } catch (\Exception $e) {
	    	Log::channel('ekm')->error($siteName.' - '.$e->getMessage());
			return abort(500);
	    }
    	// Build the XML
    	$xmlBuilder = new EkmXmlBuilder($site);
    	$xmlBuilder->addOrder($site->getOrder($id));
    	return response($xmlBuilder->asXml())
    		->header('Content-Type', 'application/xml');
	}

	/**
	 * Returns either a blank XML file,
	 * one containing all 'yet to import' EKM orders
	 * or a 500 response
	 *
	 * @param  string $siteName Must have an equivalent Ekm/Sites/* file
	 * @return xml            	Blank or containing order details
	 */
    public function getOrders($siteName, $pageNum = 1)
    {
    	session(['api_application' => 'orderwise']);

    	try{
	    	$site = SiteFactory::create($siteName);
	    } catch (\Exception $e) {
	    	Log::channel('ekm')->error($siteName.' - '.$e->getMessage());
			return abort(500);
	    }
    	// Build the XML
    	$xmlBuilder = new EkmXmlBuilder($site);

    	try{
	    	foreach($site->getOrders($pageNum) as $order) {
	    		$xmlBuilder->addOrder($order);
	    	}
	    	return response($xmlBuilder->asXml())
	    		->header('Content-Type', 'application/xml');
	    } catch (\Exception $e) {
	    	Log::channel('ekm')->error($siteName.' - '.$e->getMessage());
	    	return abort(500);
	    }
    }

    /**
     * Changes the order status to complete for all the order
     * ID's sent in the POST request
     *
     * @param  Request $request The request object
     */
    public function confirmImport(Request $request, $siteName)
    {
    	session(['api_application' => 'orderwise']);

    	try{
	    	$site = SiteFactory::create($siteName);
	    } catch (\Exception $e) {
	    	Log::channel('ekm')->error($siteName.' - '.$e->getMessage());
			return abort(500);
	    }

    	$data = explode("~",$request->input('ExportData'));

    	$prefix = $site->getAccountNumberPrefix();

    	$data = array_map(function($item) use ($prefix){
    		return preg_replace("/$prefix/", '', $item);
    	}, $data);

    	$responses = [];

		foreach ($data as $orderNumber) {
			try{
				Log::channel('ekm')->info($siteName.' - '.$site->setOrderToComplete($orderNumber));
			} catch (\Exception $e) {
				$responses = array_merge($responses, [$e->getMessage()]);
				Log::channel('ekm')->error($siteName.' - '.$e->getMessage());
			}
		}
		return $responses;
    }

    public function getOutOfStockProducts($siteName)
    {

    	session(['api_application' => 'products']);

    	try{
	    	$site = SiteFactory::create($siteName);
	    } catch (\Exception $e) {
	    	Log::channel('ekm')->error($siteName.' - '.$e->getMessage());
			return abort(500);
	    }

	    return $site->getOutOfStockProducts()->implode('<br />');
    }
}
