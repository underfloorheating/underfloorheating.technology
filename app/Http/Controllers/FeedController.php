<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\Debug\Exception\FatalThrowableError;
use App\Feed;

class FeedController extends Controller
{
	/**
	 * Output the feed data in the database
	 *
	 * @param  string $feed A string identifyinf which feed we want
	 */
    public function getFeed(string $feed)
    {
    	try {
	    	echo $this->instantiateFeed($feed)->getFeedData();
		} catch (\Throwable $e) {
	    	dd($e->getMessage());
	    	 /**
		     * We need to exchange the basic try catch block above
		     * with some kind of notifcation system
		     */
	    }
	}

    public function refreshFeed(string $feed)
    {
    	try {
	    	$this->instantiateFeed($feed)
	    		 ->saveFeedData();
	    } catch (\Throwable $e) {
	    	dd($e->getMessage());
		    /**
		     * We need to exchange the basic try catch block above
		     * with some kind of notifcation system
		     */
	    }
    }

    /**
     * Instantiates a new class given the partial class name
     *
     * @param  string 	$feed 	The first part of the name of the feed
     *                        	class we want to instantiate
     * @return Obj       		An instance of the class
     */
    private function instantiateFeed(string $feed)
    {
    	$className = "App\\Feed\\".ucfirst($feed) . 'Feed';
    	return new $className();
    }
}