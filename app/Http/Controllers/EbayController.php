<?php

namespace App\Http\Controllers;

use \DTS\eBaySDK\Shopping\Types;
use \Hkonnet\LaravelEbay\EbayServices;

class EbayController extends Controller
{
    public function competitorProducts()
    {
		/**
		* Create the service object.
		*/
		$ebay_service = new EbayServices();
		$service = $ebay_service->createShopping();

		/**
		* Create the request object.
		*/
		$request = new Types\GetSingleItemRequestType();

		// $itemIds = [
		// 	'232880387492', // 200w mats
		// 	'392103026397', // 150w mats
		// 	'322808896379', // 100w mats
		// 	'391598708867', // cable
		// 	'233123829145'  // foil
		// ];

		$itemIds = [
			'391848626045', // 200w mats
			'302394149958', // 150w mats
			'302401289631', // 100w mats
			'391848622986', // cable
			'282587334302'  // foil
		];
		$data = [];

		foreach($itemIds as $id) {
			$request->ItemID = $id;
			$request->IncludeSelector = 'Variations';
			$response = $service->getSingleItem($request)->toArray();
			if ($response['Ack'] !== 'Failure') {

				$item = $response['Item'];

				// Collate master product details
				$data[$item['ItemID']] = [
					'title' => $item['Title'],
					'primaryCategory' => [
						'id' => $item['PrimaryCategoryID'],
						'name' => $item['PrimaryCategoryName']
					],
					'image' => $item['GalleryURL'],
					'url' => $item['ViewItemURLForNaturalSearch']
				];

				$temp = [];

				// Collate variant details
				foreach($item['Variations']['Variation'] as $var) {

					$tempVar = [];

					$varTitleMat = $var['VariationSpecifics'][0]['NameValueList'][0]['Name'];
					$varTitleStat = $var['VariationSpecifics'][0]['NameValueList'][1]['Name'];

					$sizeIndex = $this->getIndex($var, $varTitleMat);
					$statIndex = $this->getIndex($var, $varTitleStat);

					$length = str_replace('m2', '', $var['VariationSpecifics'][0]['NameValueList'][$sizeIndex]['Value'][0]);
					$tempVar= [
						'price' => $var['StartPrice']['value'],
						'length' => $length,
						'stat' => $var['VariationSpecifics'][0]['NameValueList'][$statIndex]['Value'][0],
						'choices' => [
							$varTitleMat => $var['VariationSpecifics'][0]['NameValueList'][$sizeIndex]['Value'][0],
							$varTitleStat => $var['VariationSpecifics'][0]['NameValueList'][$statIndex]['Value'][0]
						]
					];
					$temp[$length][] = $tempVar;
				}

				// Sort variants into correct order
				ksort($temp);
				$variants = [];
				foreach($temp as &$opts) {
					usort($opts, function($a, $b){
						return $a['price'] <=> $b['price'];
					});
					$variants = array_merge($variants, $opts);
				}

				// Save sorted variants against master product details
				$data[$item['ItemID']]['variants'] = $variants;
			}
		}
		return view('competitor-products', ['items' => $data]);
    }

    /**
     * [getIndex description]
     * @param  [type] $arr  [description]
     * @param  [type] $type [description]
     * @return [type]       [description]
     */
    private function getIndex($arr, $type)
    {
    	foreach($arr['VariationSpecifics'][0]['NameValueList'] as $key => $a) {
    		if($a['Name'] == $type) {
    			return $key;
    		}
    	}
    	return false;
    }

}