<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Ufhd\Quote;
use App\Ufhd\Quotefile;
use App\Ufhd\Mail\QuoteRequest;

use Mail;
use Redirect;

class UfhdController extends Controller
{
	const STORAGE_PATH = 'quote-files';

	/**
	 * Display the Request A Quote form
	 */
    public function showQuoteForm()
    {
    	return response()
    		->view('ufhd.request-a-quote.form')
    		->header('P3P', 'CP="CURa ADMa DEVa PSAo PSDo OUR BUS UNI PUR INT DEM STA PRE COM NAV OTC NOI DSP COR"');
    }

    /**
     * Send the quote request details to the sales team
     *
     * @param  Request $request The incoming request
     */
    public function submitQuote(Request $request)
    {
    	$validatedData = $request->validate([
			'first_name' => 'required',
			'last_name' => 'required',
			'email' => 'required|email',
			'telephone' => 'nullable',
			'add1' => 'required',
			'add2' => 'nullable',
			'town' => 'nullable',
			'county' => 'nullable',
			'postcode' => 'required',
			'company_name' => 'nullable',
			'vat_number' => 'nullable',
			'details' => 'required',
			'system_type' => 'required'
	    ]);

    	// Save the quote
    	$quote = Quote::create($request->all());

    	// Save any uploaded files
    	$files = [];
    	foreach($request->file() as $id => $uploadedFile) {
    		// Move the uploaded file to a safe lcoation and hash the filename
    		$hashedFilename = $uploadedFile->store(self::STORAGE_PATH);
    		// Create a DB entry for the file
    		$files[] = Quotefile::create([
				'file_name' => str_replace(self::STORAGE_PATH.'/', '', $hashedFilename),
				'file_ext' => $uploadedFile->getClientOriginalExtension(),
				'file_type' => $uploadedFile->getClientMimeType(),
				'upload_name' => $uploadedFile->getClientOriginalName(),
				'quote_id' => $quote->id
    		]);
    	}

    	Mail::to(env('UFHD_QUOTE_EMAIL'))
    		->bcc('chris@theunderfloorheatingstore.com')
    		->send(new QuoteRequest($quote, $files));

        return view('ufhd.request-a-quote.success');
    }

    /**
     * Forces the download of a requested file
     *
     * @param  string  $filenameOnDisk   The hashed name of the file we want to download
     * @param  string  $downloadFilename The filename we want to use when downloading the file
     */
    public function downloadFile(Quote $quote, Quotefile $quoteFile)
    {
    	// Make sure quote and quotefile are related as security measure
    	$file = storage_path().'/app/'.self::STORAGE_PATH.'/'.$quoteFile->file_name;
    	if($quoteFile->quote_id == $quote->id && file_exists($file)) {
    		return response()->download($file, urldecode($quoteFile->upload_name));
    	}
    	return view('ufhd.file-download-error');
    }
}