<?php

namespace App\Http\Controllers;

use App\Jobs\StoreRegion;
use App\Merchants\Merchant;
use App\Merchants\Region;
use App\Merchants\Sources;
use App\Postcode;
use GuzzleHttp\Client;

class MerchantsController extends Controller
{
    public function index()
    {
    	return 'You must supply a source string in the URL to indicate which source you wish to extract merchants from; /merchants/{source}';
    }

    /**
     * Queues up a Job for each region in the config.  This allows multiple
     * calls to third party search forms without timing out.
     *
     * @param  string  $source The Source class name we want to work with
     * @param  integer $offset An offset for where to start in the regions config array
     * @param  integer $limit  A limit to how many entries from the regions config array we want to us
     * @return string          A short messahe to indicate how may jobs were queued up
     */
    public function downloadMerchantData($source, $offset = 0, $limit = 4000, $shortList = false)
	{
		// check we have a Merchant Source matching the string provided
		$sourceName = 'App\\Merchants\\Sources\\'.ucWords($source);
		if(!class_exists($sourceName)) {
			return 'There is no merchant source named "' . ucWords($source) . '"';
		}
		$postcodes = $shortList ? 'postcode_regions_short_list' : 'postcode_regions';
		// Get a subset of regions we want to search against
		$regions = array_slice(config('tools.'.$postcodes), $offset, $limit);

		// Queue up a Job for each lookup task
		$jobCount = 0;
		foreach($regions as $region) {
			StoreRegion::dispatch($sourceName, $region);
			$jobCount++;
		}

		return "Added ".$jobCount." jobs to the queue";
	}

    /**
     * Queues up a Job for each region in the config.  This allows multiple
     * calls to third party search forms without timing out.
     *
     * @param  string  $source The Source class name we want to work with
     * @param  integer $offset An offset for where to start in the regions config array
     * @param  integer $limit  A limit to how many entries from the regions config array we want to us
     * @return string          A short messahe to indicate how may jobs were queued up
     */
    public function downloadMerchantDataUsingPostcodeDatabase($source, $offset = 0, $limit = 4000)
	{
		// check we have a Merchant Source matching the string provided
		$sourceName = 'App\\Merchants\\Sources\\'.ucWords($source);
		if(!class_exists($sourceName)) {
			return 'There is no merchant source named "' . ucWords($source) . '"';
		}
		// Pull a single postcode record for each region from the DB
		$regions = Postcode::select('postcode','postcode_district AS code','longitude AS lng','latitude AS lat')
			->where('status', 'live')
			->distinct()
			->groupBy('postcode_area')
			->offset($offset)
			->limit($limit)
			->get()
			->toArray();

		// Queue up a Job for each lookup task
		$jobCount = 0;
		foreach($regions as $region) {
			if($region['code'] != null) {
				StoreRegion::dispatch($sourceName, $region);
				$jobCount++;
			}
		}

		return "Added ".$jobCount." jobs to the queue";
	}

	/**
	 * This is a place you can run each individual source without affecting
	 * the database to check the parsing algorithm
	 */
	public function testDownload()
	{
		$source = new Sources\Warmup(new Client);

		$region = ['code' => 'SS6', 'postcode' => 'SS69RY', 'lat' => 51.58746, 'lng' => 0.60872];

        dd($source->getDataForRegion(new Region($region)));
	}

	/**
	 * Us this when the source model grabs the data from a location other
	 * than the regions config array
	 *
	 * @param  string $source The name of the source model we want to run
	 */
	public function getDataFromSource($source)
	{
		// check we have a Merchant Source matching the string provided
		$sourceName = 'App\\Merchants\\Sources\\'.ucWords($source);
		if(!class_exists($sourceName)) {
			return 'There is no merchant source named "' . ucWords($source) . '"';
		}

		$source = new $sourceName(new Client);

		$results = [];
		foreach($source->getData() as $details) {
			$merchant = Merchant::firstOrCreate($details);
			if($merchant->wasRecentlyCreated) {
				$results[] = 'added ' . $merchant->company;
			}
		}
		dd($results);
	}
}
