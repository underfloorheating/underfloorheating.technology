<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\DiscountMyQuote\Quote;
use DotMailer\Api\DataTypes;

use Illuminate\Support\Collection ;
use League\Csv\Writer;
use SplTempFileObject;

use DB;

class DiscountMyQuoteController extends Controller
{
	const ADDRESS_BOOK_ID = '15099331';
	const TEMPLATE_ID = 1116496;

	/**
	 * System dashboard
	 */
	public function index()
	{
		return view('dotmailer.discount-my-quote.home');
	}

	/**
	 * Show a form button for the user to click to refresh the data
	 */
	public function requestConfirmationForRefresh()
	{
		return view('dotmailer.discount-my-quote.data.refresh');
	}

	/**
	 * Grab the data from the Google Sheet and push it into DotMailer
	 */
	public function refreshData()
	{
		try{
			Quote::saveDataFromGoogleSheet();

			$dotmailer = app()->make('dotmailer');

			$columnHeaders = [
				'email',
				'acc_man_first_name',
				'acc_man_last_name',
				'acc_man_tel',
				'acc_man_email'
			];

			$data = [];
			// Upload all the data we pulled from the Google Sheet into the address book in Dotmailer
			Quote::with('salesman')
				->groupBy('email')
				->orderBy('order_date', 'DESC')
				->get()
				->each(function($quote, $key) use (&$data, $columnHeaders){
					if($quote->salesman != NULL) {
						$data[] = array_combine($columnHeaders, [
							$quote->email,
							$quote->salesman->first_name,
							$quote->salesman->last_name,
							$quote->salesman->telephone,
							$quote->salesman->email
						]);
					}
				});

			// Delete all existing contacts from the address book
			$dotmailer->DeleteAddressBookContacts(self::ADDRESS_BOOK_ID);

			$status = $dotmailer->PostAddressBookContactsImport(
				self::ADDRESS_BOOK_ID,
				new DataTypes\ApiFileMedia(
					[
						'FileName' => 'datafile.csv',
						'Data' => base64_encode($this->createCsv($data, $columnHeaders)->getContent())
					]
				)
			);

			// Reset the last updated time in the database
			$this->lastUpdated(new \DateTime());

			return view('dotmailer.discount-my-quote.data.successfully-refreshed');

		} catch (\Exception $e) {
			echo $e->getMessage();
		}
		return '' ;
	}

	/**
	 * Collate and serve the HTML content for the DotMailer Discount My Quote campaign.
	 *
	 * @param  string 	$email 	The email address of the user we are sending the current email to
	 * @return mixed 			Either a 200 code with the HTML or a 204 code with no content
	 */
	public function generateQuoteHTMLForEmail($email)
	{
		$quoteObjects = Quote::where('email', $email)->get();

		if($quoteObjects->count() == 0) {
			// If we have no data, send the 204 status code the DotMailer
			// External Dynamic Content Block widget expects.
			// https://support.dotmailer.com/hc/en-gb/articles/212212948-Working-with-external-dynamic-content
			return response('', 204);
		}

		$quotes['discountable'] = $quoteObjects->filter(function($quote, $key){
			return $quote->available_discount > 0;
		});
		$quotes['maximum_discount_applied'] = $quoteObjects->filter(function($quote, $key){
			return $quote->available_discount <= 0;
		});

		return view('dotmailer.discount-my-quote.email.content', [
			'quotes' => $quotes,
			'quoteCount' => $quoteObjects->count()
		]);
	}

	/**
	 * A function to generate a CSV for a given model collection.
	 *
	 * @param Collection $modelCollection
	 * @param $tableName
	 */
	private function createCsv(array $data, array $columnHeaders)
	{
		$csv = Writer::createFromFileObject(new SplTempFileObject());
		// This creates header columns in the CSV file - probably not needed in some cases.
		$csv->insertOne($columnHeaders);
		foreach ($data as $row){
			$csv->insertOne($row);
		}
		return $csv;
	}

	/**
	 * Request final confirmation from the user that it's OK to send this campaign
	 */
	public function requestConfirmationToSendCampaign()
	{
		// If the last updated date is more than 1 week ago, we need to refresh the data
		if($this->lastUpdated()->diff(new \DateTime())->days > 7) {
			return view('dotmailer.discount-my-quote.data.needs-refreshing');
		}

		// Grab the address book from dotmailer so we can see if it has been populated with data yet
		$dotmailer = app()->make('dotmailer');
		$addressBook = $dotmailer->GetAddressBookById(self::ADDRESS_BOOK_ID);

		if($addressBook->contacts->__toString() == 0) {
			return view('dotmailer.discount-my-quote.data.no-data');
		}

		return view('dotmailer.discount-my-quote.send.form', ['numContacts' => $addressBook->contacts->__toString()]);
	}

	/**
	 * Copy the Discount My Quote campaign template into a new campaign and
	 * end it to the Discount My Quote address book in Dotmailer.
	 */
	public function sendCampaign()
	{
		$dotmailer = app()->make('dotmailer');

		// Copy campaign from template
		$template = $dotmailer->GetTemplateById(new DataTypes\XsInt(self::TEMPLATE_ID));

		// Use copied campaign details to create new campaign
		$campaign = $dotmailer->PostCampaigns(new DataTypes\ApiCampaign($template->toArray()));

		$campaignSend = new DataTypes\ApiCampaignSend([
			'CampaignID' => $campaign->id->__toString(),
			'AddressBookIDs' => [self::ADDRESS_BOOK_ID],
			'SendDate' => (new \DateTime())->format('Y-m-d\T18:00:00')
		]);

		// Send campaign to DMQ address book
		$response = $dotmailer->PostCampaignsSend($campaignSend);

		return view('dotmailer.discount-my-quote.send.success');
	}

	/**
	 * Gets or sets the last_updated value in the database
	 *
	 * @param  \DateTime|null $date The date we want to set
	 * @return \DateTime            A DateTime object representing the last update date
	 */
	private function lastUpdated(\DateTime $date = NULL)
	{
		$table = 'dmq_last_update';

		if($date == NULL) {
			$value = DB::table($table)->first();
			return new \DateTime($value->last_updated);
		} else {
			DB::table($table)->truncate();
			DB::table($table)->insert(['last_updated' => $date->format('Y-m-d H:i:s')]);
		}
	}

	/**
	 * Display a mock up of how the main email will look when sent to customers
	 */
	public function viewEmail($email = null)
	{
		$quoteObjects = Quote::where('email', $email)->get();

		if($quoteObjects->count() == 0) {
			$discountable = new Quote();
			$discountable->salesman_username = 'Dean';
			$discountable->order_number = '123456';
			$discountable->customer_order_ref = 'Bathroom';
			$discountable->order_date = '2019-01-01';
			$discountable->order_net = '1500';
			$discountable->name = 'Test customer';
			$discountable->email = 'test@customer.com';
			$discountable->discount_applied = '0.12';
			$discountable->available_discount = '0.08';

			$maxApplied = new Quote();
			$maxApplied->salesman_username = 'Sam';
			$maxApplied->order_number = '456789';
			$maxApplied->customer_order_ref = 'Bedroom';
			$maxApplied->order_date = '2019-01-01';
			$maxApplied->order_net = '1800';
			$maxApplied->name = 'Test customer';
			$maxApplied->email = 'test@customer.com';
			$maxApplied->discount_applied = '0.20';
			$maxApplied->available_discount = '0';

			$quotes['discountable'] = [$discountable];
			$quotes['maximum_discount_applied'] = [$maxApplied];

			$quoteCount = 2;

			$salesman = $discountable->salesman;

		} else {
			$quotes['discountable'] = $quoteObjects->filter(function($quote, $key){
				return $quote->available_discount > 0;
			});
			$quotes['maximum_discount_applied'] = $quoteObjects->filter(function($quote, $key){
				return $quote->available_discount <= 0;
			});
			$quoteCount = $quoteObjects->count();

			$salesman = $quoteObjects[0]->salesman;
		}

		$content = view('dotmailer.discount-my-quote.email.content', [
			'quotes' => $quotes,
			'quoteCount' => 2
		]);

		$content .= view('dotmailer.email.signatures.personalised.ufhs', [
			'firstName' => $salesman->first_name,
			'lastName' => $salesman->last_name,
			'telephone' => $salesman->telephone
		]);

		return view('dotmailer.discount-my-quote.email-example', ['content' => $content]);
	}
}