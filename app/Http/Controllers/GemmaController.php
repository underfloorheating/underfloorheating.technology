<?php

namespace App\Http\Controllers;

use App\Jobs\DownloadAsclEmail;
use App\Jobs\DownloadPoliticianEmail;
use App\Jobs\DownloadThurrockCouncillorEmail;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use League\Csv\Writer;
use Sunra\PhpSimple\HtmlDomParser;

class GemmaController extends Controller
{
    public function getLabourCandidates()
	{
		$details = [];
		$html = HtmlDomParser::str_get_html(file_get_contents('http://vote.labour.org.uk/all-candidates'));
		foreach($html->find('.card > a') as $link) {
			$locationObj = $link->find('.card-title', 0);
			$nameObj = $link->find('.card-meta', 0);

			DownloadPoliticianEmail::dispatch(
				$locationObj ? $locationObj->plaintext : '',
				$nameObj ? $nameObj->plaintext : '',
				'http://vote.labour.org.uk'.$link->href
			);
		}
		return '';
	}

	public function getThurrockCouncillors()
	{
		$details = [];
		$html = HtmlDomParser::str_get_html(file_get_contents('https://democracy.thurrock.gov.uk/mgFindMember.aspx?XXR=0&AC=PARTY&PID=24'));
		foreach($html->find('.mgThumbsList li') as $li) {
			DownloadThurrockCouncillorEmail::dispatch(
				'https://democracy.thurrock.gov.uk/' . $li->find('a', 0)->href,
				$li->find('a', 1)->plaintext,
				$li->find('p', 0)->plaintext
			);
		}
		return '';
	}

	public function getAsclMembers()
	{
		$details = [];
		$html = HtmlDomParser::str_get_html(file_get_contents('https://www.ascl.org.uk/About-us/Organisation-and-Governance/Local-Representatives/Contact-your-ASCL-Local-Representative'));
		foreach($html->find('a.text-secondary') as $link) {
			DownloadAsclEmail::dispatch(
				'https://www.ascl.org.uk' . $link->href,
				$link->plaintext
			);
		}
		return '';
	}
}
