<?php

namespace App\Http\Middleware;

use Closure;

use App\Models\Platform;

class SetPlatform
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
    	$request->session()->put(
    		'platform',
    		Platform::where(
    			'name',
    			$request->route()->getPrefix()
    		)
    	);
        return $next($request);
    }
}
