<?php

namespace App\Http\Middleware;

use Closure;

class RestrictToUnderfloorOffice
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
    	$allowedIPs = [
    		'127.0.0.1',
    		'195.171.215.242',
    		'51.148.184.227'
    	];

    	if (!in_array($_SERVER['REMOTE_ADDR'], $allowedIPs)) {
            return abort(403, 'You are not authorized to access this');
        }

        return $next($request);
    }
}
