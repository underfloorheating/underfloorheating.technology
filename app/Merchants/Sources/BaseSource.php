<?php

namespace App\Merchants\Sources;

use GuzzleHttp\Client;

class BaseSource
{
	public function __construct()
	{
		$this->client = new Client;
	}
}