<?php

namespace App\Merchants\Sources;

use App\Merchants\Region;

interface SourceInterface
{
	public function getDataForRegion(Region $region);
}