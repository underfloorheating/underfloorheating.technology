<?php

namespace App\Merchants\Sources;

use App\Merchants\Merchant;
use App\Merchants\Region;
use GuzzleHttp\Client;

class Graham extends BaseSource implements SourceInterface
{
	private const SEARCH_URL = 'https://www.grahamdirect.co.uk/wp-admin/admin-ajax.php';
	private const SOURCE_NAME = 'Graham Direct';

	/**
	 * @var GuzzleHttp\Client
	 */
	private $client;

	public function __construct(Client $client)
	{
		$this->client = $client;
	}

	public function getDataForRegion(Region $region)
	{
		$response = $this->client->request('GET', self::SEARCH_URL, [
			'query' => [
				'action' => 'store_search',
				'lat' => $region->getLat(),
				'lng' => $region->getLng(),
				'max_results' => 100,
				'search_radius' => 100
			],
			'verify' => false
		]);

		return $this->parseResponse(json_decode((string) $response->getBody()));
	}

	private function parseResponse($data)
	{
		$mappedData = array_map(function($item){
			return [
				'company' => html_entity_decode($item->store),
				'address_1' => $item->address,
				'address_2' => $item->address2,
				'town' => $item->city,
				'county' => $item->state,
				'postcode' => $item->zip,
				'email' => $item->email,
				'telephone' => $item->phone,
				'fax' => $item->fax,
				'source' => self::SOURCE_NAME
			];
		}, $data);
		return $mappedData;
	}
}