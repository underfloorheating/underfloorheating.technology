<?php

namespace App\Merchants\Sources;

use App\Merchants\Region;
use GuzzleHttp\Client;

class Edmundson implements SourceInterface
{
	private const SEARCH_URL = 'http://www.edmundson-electrical.co.uk/local_locations';
	private const SOURCE_NAME = 'Edmundson Electrical';

	private $client;

	public function __construct(Client $client)
	{
		$this->client = $client;
	}

	public function getDataForRegion(Region $region)
	{
		$response = $this->client->request('GET', self::SEARCH_URL, [
			'query' => [
				'latitude' => $region->getLat(),
				'longitude' => $region->getLng()
			],
			'headers' => [
				'Host' => "www.edmundson-electrical.co.uk",
				'Connection' => "keep-alive",
				'Accept' => "application/json, text/javascript, */*; q=0.01",
				'X-Requested-With' => "XMLHttpRequest",
				'User-Agent' => "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
				'Referer' => "http://www.edmundson-electrical.co.uk/locations",
				'Accept-Encoding' => "gzip, deflate",
				'Accept-Language' => "en-GB,en;q=0.9",
				'Cookie' => '_ga=GA1.3.1934837661.1582723050; CAKEPHP=b8144jls64ik3br5pus0q84251; CookieControl={"necessaryCookies":[],"optionalCookies":{"analytics":"accepted"},"initialState":{"type":"closed"},"statement":{"shown":true,"updated":"22/05/2018"},"consentDate":1582723050341,"consentExpiry":90,"interactedWith":true,"user":"486CB6E6-DA13-491A-B608-546C5D8EB6C7"}; _gid=GA1.3.629599630.1583145595; _gat=1'
			]
		]);

		return $this->parseResponse((string) $response->getBody());
	}

	private function parseResponse($jsonString)
	{
		$dataArray = json_decode($jsonString);

		$returnArray = [];

		foreach($dataArray->data as $data) {
			$returnArray[] = [
				'email' => $data->sl->email,
				'telephone' => $data->sl->tel,
				'fax' => $data->sl->fax,
				'postcode' => $data->sl->postcode,
				'company' => $data->sl->name,
				'county' => $data->sl->county,
				'town' => $data->sl->town,
				'address_3' => $data->sl->addr3,
				'address_2' => $data->sl->addr2,
				'address_1' => $data->sl->addr1,
				'contact' => $data->sl->manager,
				'source' => self::SOURCE_NAME
			];
		}

		return $returnArray;
	}
}