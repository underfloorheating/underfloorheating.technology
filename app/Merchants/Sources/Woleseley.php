<?php

namespace App\Merchants\Sources;

use App\Merchants\Region;
use GuzzleHttp\Client;
use Sunra\PhpSimple\HtmlDomParser;
use Symfony\Component\Debug\Exception\FatalThrowableError;

class Woleseley implements SourceInterface
{
	private const SEARCH_URL = 'https://www.wolseley.co.uk/webapp/wcs/stores/servlet/BranchSearchList';
	private const SOURCE_NAME = 'Woleseley';

	private $client;

	public function __construct(Client $client)
	{
		$this->client = $client;
	}

	public function getDataForRegion(Region $region)
	{
		$response = $this->client->request('POST', self::SEARCH_URL, [
			'query' => [
				'storeId' => 10203,
				'langId' => 44,
				'catalogId' => 12001,
				'coordinates' => $region->getLat().','.$region->getLng(),
				'fromPage' => 'StoreLocator',
				'storeSearchText' => $region->getCode(),
				'distanceRange' => 10
			]
		]);
		$body = preg_replace(['/\\n/','/^\/\*/', '/\*\/$/'], "", (string) $response->getBody());
		return $this->parseResponse(json_decode($body));
	}

	private function parseResponse($data)
	{

		$mappedData = array_map(function($item){
			return [
				'company' => $item->name,
				'address_1' => $item->address1,
				'address_2' => $item->address2,
				'town' => $item->city,
				'county' => $item->state,
				'postcode' => $item->postalCode,
				'telephone' => $item->telephoneNumber,
				'fax' => $item->fax,
				'contact' => ucwords(strtolower($item->managerName)),
				'source' => self::SOURCE_NAME
			];
		}, $data->storeLocationsJson);
		return $mappedData;
	}
}