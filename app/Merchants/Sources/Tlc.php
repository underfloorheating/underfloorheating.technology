<?php

namespace App\Merchants\Sources;

use GuzzleHttp\Client;
use Sunra\PhpSimple\HtmlDomParser;

class Tlc
{
	private const HOST = "https://www.tlc-direct.co.uk";
	private const SEARCH_URL = 'https://www.tlc-direct.co.uk/Information/branches/';
	private const SOURCE_NAME = 'TLC Direct';

	private $client;

	public function __construct(Client $client)
	{
		$this->client = $client;
	}

	public function getData()
	{
		$response = $this->client->request('GET', self::SEARCH_URL);

		$urls = $this->parseBranchPageUrls((string) $response->getBody());

		foreach($urls as $url) {
			$response = $this->client->request('GET', $url);
			$details[] = $this->parseBranchDetails((string) $response->getBody());
		}
		return $details;
	}

	private function parseBranchPageUrls($html)
	{
		$dom = HtmlDomParser::str_get_html($html);
		$urls = [];
		foreach($dom->find('li.branch-list__branch a') as $a) {
			$urls[] = $a->href;
		}
		return array_map(function($url){
			return self::HOST . $url;
		}, $urls);
	}

	private function parseBranchDetails($html)
	{
		$data = [];

		$dom = HtmlDomParser::str_get_html($html);
		$address = trim($dom->find('.column--branch-address .branch-info-item__text')[0]->plaintext);
		$data['company'] = trim($dom->find('.branch-title')[0]->plaintext);
		$data['telephone'] = trim($dom->find('.column--branch-phone .branch-info-item__text')[0]->plaintext);
		$data['fax'] = trim($dom->find('.column--branch-fax .branch-info-item__text')[0]->plaintext);
		$data['email'] = trim($dom->find('.column--branch-email .branch-info-item__text')[0]->plaintext);

		$address = array_reverse(explode("\r\n", $address));

		$address = array_map(function($item){
			if(preg_match('/[\(\)]/', $item) == 1) {
				return false;
			}
			return preg_replace(['/,/','/\./'], '', $item);
		}, $address);

		$keys = ['postcode','county','town','address_3','address_2','address_1'];

		for($x=0; $x<count($address); $x++) {
			if($x == count($address) - 1 && $x != count($keys) - 1) {
				$data[$keys[count($keys) - 1]] = trim($address[$x]);
			} else {
				$data[$keys[$x]] = trim($address[$x]);
			}
		}
		$data['source'] = self::SOURCE_NAME;
		return $data;
	}
}