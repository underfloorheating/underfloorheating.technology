<?php

namespace App\Merchants\Sources;

use App\Merchants\Merchant;
use App\Merchants\Region;
use GuzzleHttp\Client;
use Sunra\PhpSimple\HtmlDomParser;

class Jewson extends BaseSource implements SourceInterface
{
	private const HOST = "https://www.jewson.co.uk";
	private const SEARCH_URL = 'https://www.jewson.co.uk/branch-locator/';
	private const SOURCE_NAME = 'Jewson';

	private $client;

	public function __construct(Client $client)
	{
		$this->client = $client;
	}

	public function getDataForRegion(Region $region)
	{
		$response = $this->client->request('GET', self::SEARCH_URL, [
			'query' => [
				'Location' => $region->getCode()
			]
		]);

		$urls = $this->parseBranchPageUrls((string) $response->getBody());

		foreach($urls as $url) {
			$response = $this->client->request('GET', $url);
			$details[] = $this->parseBranchDetails((string) $response->getBody());
		}
		return $details;
	}

	private function parseBranchPageUrls($html)
	{
		$dom = HtmlDomParser::str_get_html($html);
		$urls = [];
		foreach($dom->find('.full-details-link') as $a) {
			$urls[] = self::HOST . $a->href;
		}
		return $urls;
	}

	private function parseBranchDetails($html)
	{
		$data = [];

		$dom = HtmlDomParser::str_get_html($html);

		$company = $dom->find('[id$="BranchTitle"]', 0);
		$telephone = $dom->find('span[itemprop="telephone"] a', 0);
		$fax = $dom->find('span[itemprop="faxNumber"] a', 0);
		$email = $dom->find('[id$="BranchEmail"]', 0);
		$contact = $dom->find('[id$="BranchManager"]', 0);

		$postcode = $dom->find('.branch-details-group span[itemprop="postalCode"]', 0);
		$county = $dom->find('.branch-details-group span[itemprop="addressRegion"]', 0);
		$town = $dom->find('.branch-details-group span[itemprop="addressLocality"]', 0);
		$address = $dom->find('.branch-details-group div[itemprop="streetAddress"]', 0);

		$data['company'] = $company ? 'Jewsons - ' . trim($company->plaintext) : null ;
		$data['telephone'] = $telephone ? trim($telephone->plaintext) : null ;
		$data['fax'] = $fax ? trim($fax->plaintext) : null ;
		$data['email'] = $email ? trim($email->plaintext) : null ;
		$data['contact'] = $contact ? trim($contact->plaintext) : null ;

		$data['postcode'] = $postcode ? trim($postcode->plaintext) : null ;
		$data['county'] = $county ? trim($county->plaintext) : null ;
		$data['town'] = $town ? trim($town->plaintext) : null ;

		if($address) {
			$address = explode("\r\n", trim($address->plaintext));
			for($x=0; $x<count($address); $x++) {
				$y = $x + 1;
				$data['address_' . $y] = $address[$x];
			}
		}
		$data['source'] = self::SOURCE_NAME;
		return $data;
	}
}