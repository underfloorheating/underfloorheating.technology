<?php

namespace App\Merchants\Sources;

use App\Merchants\Merchant;
use App\Merchants\Region;
use GuzzleHttp\Client;
use Sunra\PhpSimple\HtmlDomParser;

class Warmup extends BaseSource implements SourceInterface
{
	private const HOST = "https://www.warmup.co.uk";
	private const SEARCH_URL = 'https://www.warmup.co.uk/store-locator-temp/getstore_locator.php';
	private const SOURCE_NAME = 'Warmup';

	private $client;

	public function __construct(Client $client)
	{
		$this->client = $client;
	}

	public function getDataForRegion(Region $region)
	{
		$response = $this->client->request('GET', self::SEARCH_URL, [
			'query' => [
				'postcode' => $region->getPostcode()
			]
		]);

		return $this->parseResponse(new \SimpleXMLElement((string) $response->getBody()));
	}

	private function parseResponse($data)
	{
		return array_map(function($xmlItem){
			$address = explode(",", (string) $xmlItem['address']);

			$address = array_map(function($item) use ($xmlItem) {
				return trim(preg_replace('/'.(string) $xmlItem['postal'].'/','',$item));
			}, $address);

			$data['town'] = array_pop($address);

			for($x=0; $x<count($address); $x++) {
				$y = $x + 1;
				$data['address_' . $y] = $address[$x];
			}
			return array_merge($data, [
				'company' => (string) $xmlItem['name'],
				'county' => '',
				'postcode' => (string) $xmlItem['postal'],
				'email' => '',
				'telephone' => (string) $xmlItem['phone'],
				'fax' => '',
				'source' => self::SOURCE_NAME
			]);
		}, $data->xpath('marker'));
	}
}