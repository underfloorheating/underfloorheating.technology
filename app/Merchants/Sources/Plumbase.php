<?php

namespace App\Merchants\Sources;

use GuzzleHttp\Client;

class Plumbase
{
	private const SEARCH_URL = 'https://www.plumbase.co.uk/storefinder/stores';
	private const SOURCE_NAME = 'Plumbase';

	private $client;

	public function __construct(Client $client)
	{
		$this->client = $client;
	}

	public function getData()
	{
		$response = $this->client->request('POST', self::SEARCH_URL);

		return $this->parseResponse(json_decode((string) $response->getBody()));
	}

	private function parseResponse($data)
	{
		$mappedData = array_map(function($item){
			return [
				'company' => $item->LocationName,
				'address_1' => $item->Address1,
				'address_2' => $item->Address2,
				'town' => $item->Address4 == "" ? "" : $item->Address3,
				'county' => $item->Address4 != "" ? $item->Address4 : $item->Address3,
				'postcode' => $item->postcode,
				'email' => $item->Email,
				'telephone' => $item->Phone,
				'fax' => $item->Fax,
				'source' => self::SOURCE_NAME
			];
		}, $data->all);
		return $mappedData;
	}
}