<?php

/**
 * Scraping of data for this source is done using the NPM Puppeteer module in a different project
 */
namespace App\Merchants\Sources;

use App\Merchants\Merchant;
use App\Merchants\Region;
use GuzzleHttp\Client;
use Sunra\PhpSimple\HtmlDomParser;

class Cef
{
	private const SEARCH_URL = 'https://www.cef.co.uk/stores/directory';
	private const SOURCE_NAME = 'CEF';

	private $client;

	public function __construct(Client $client)
	{
		$this->client = $client;
	}


}