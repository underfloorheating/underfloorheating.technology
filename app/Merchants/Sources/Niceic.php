<?php

namespace App\Merchants\Sources;

use App\Merchants\Region;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;
use Sunra\PhpSimple\HtmlDomParser;

class Niceic implements SourceInterface
{
	private const SEARCH_URL = 'https://proximity.niceic.com/mainform.aspx';
	private const SOURCE_NAME = 'Nic Eic';

	private $client;

	public function __construct(Client $client)
	{
		$this->client = $client;
	}

	/**
	 * Grabs HTML from search page and returns parsed data
	 *
	 * @param  string $postcode The postcode we are currently searching with
	 * @return array            An array of the data we managed to find
	 */
	public function getDataForRegion(Region $region)
	{
		$response = $this->client->request('GET', self::SEARCH_URL, [
			'query' => [
				'PostCode' => $region->getCode()
			]
		]);
		return $this->parseHtml((string) $response->getBody());
	}

	/**
	 * Extracts the data we want from the HTML and returns an associative array
	 * ready for the Merchant Object
	 *
	 * @param  string $html An HTML string
	 * @return array       	Array containing associative arrays
	 */
	private function parseHtml($html)
	{
		$dom = HtmlDomParser::str_get_html($html);
		$pattern = [
			'/\\r\\n/',
			'/\\t/',
			'/\s{2,}/'
		];
		$replace = [
			'',
			'',
			'~'
		];
		$data = [];
		foreach($dom->find('table[id="gvContractors"] tr') as $rawRow) {
			if($cell = $rawRow->find('td', 0)) {
				$text = preg_replace($pattern, $replace, trim($cell->plaintext));
				if($text != "Next" && $text != "") {
					$data[] = $this->getAssoc(explode('~',$text));
				}
			}
		}
		return $data;
	}

	/**
	 * Converts the flat array derided from the HTML into an associative array
	 * we can use to create a new Merchant object
	 *
	 * @param  array $details The flat data array
	 * @return array          An associative array with correct keys and data associations
	 */
	public function getAssoc($details)
	{
		$data = [];

		$data['email'] = $this->setDataAndClear($details, '/@/');
		$data['telephone'] = $this->setDataAndClear($details, '/^(((\+44\s?\d{4}|\(?0\d{4}\)?)\s?\d{3}\s?\d{3})|((\+44\s?\d{3}|\(?0\d{3}\)?)\s?\d{3}\s?\d{4})|((\+44\s?\d{2}|\(?0\d{2}\)?)\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$/');
		$data['postcode'] = $this->setDataAndClear($details, '/^(([gG][iI][rR] {0,}0[aA]{2})|((([a-pr-uwyzA-PR-UWYZ][a-hk-yA-HK-Y]?[0-9][0-9]?)|(([a-pr-uwyzA-PR-UWYZ][0-9][a-hjkstuwA-HJKSTUW])|([a-pr-uwyzA-PR-UWYZ][a-hk-yA-HK-Y][0-9][abehmnprv-yABEHMNPRV-Y]))) {0,}[0-9][abd-hjlnp-uw-zABD-HJLNP-UW-Z]{2}))$/');

		$data['company'] = array_shift($details);

		// Trim any web addresses from the remaining data
		$details = array_filter($details, function($item){
			return !preg_match('/(\.co|\.net|\.org|www|http)/', $item);
		});

		// The last entry in the array should now be the county
		$county = array_pop($details);
		$data['county'] = $county;

		// Some of the remaining array values are comma separated address lines,
		// so we'll split them out and flatten them so we can apply them to the
		// correct fields.
		$details = array_map(function($item){
			return explode(',', $item);
		}, $details);

		$details = $this->flatten($details);

		// Assign the last remaining fields
		$fields = ['town', 'address_2', 'address_1'];
		$details = array_reverse($details);

		for($x = 0; $x < count($details); $x++) {
			if(isset($fields[$x])) {
				$fieldName = $fields[$x];
				if($x == count($details) - 1 && $fieldName != 'address_1') {
					$data['address_1'] = $details[$x];
				} else {
					$data[$fieldName] = $details[$x];
				}
			}
		}

		$data['source'] = self::SOURCE_NAME;

		return $data;
	}

	/**
	 * Extracts the required data using a regex match and deletes the
	 * entry from the main array
	 *
	 * @param array &$details The main data array
	 * @param string $regex    The regex pattern to match the correct entry
	 */
	private function setDataAndClear(&$details, $regex)
	{
		$data = preg_grep($regex, $details);
		unset($details[$this->array_key_first($data)]);
		return array_pop($data);
	}

	/**
	 * polyfill for PHP 7.3 array_key_first function
	 *
	 * @param  array  $arr The array we want the first key from
	 * @return string      The key value
	 */
	private function array_key_first(array $arr) {
		foreach($arr as $key => $unused) {
			return $key;
		}
		return NULL;
	}

	/**
	 * Recursively flatten array
	 *
	 * @param  array  $array 	The array we want to flatten
	 * @return array        	The flattened array
	 */
	private function flatten(array $array) {
		$return = [];
		array_walk_recursive($array, function($a) use (&$return) {
			$return[] = $a;
		});
		return $return;
	}
}