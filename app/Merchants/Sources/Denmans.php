<?php

namespace App\Merchants\Sources;

use App\Merchants\Merchant;
use App\Merchants\Region;
use GuzzleHttp\Client;

class Denmans extends BaseSource implements SourceInterface
{
	private const SEARCH_URL = 'https://www.denmans.co.uk/den/store-finder/findNearbyStores';
	private const SOURCE_NAME = 'Denmans';

	private $client;

	public function __construct(Client $client)
	{
		$this->client = $client;
	}

	public function getDataForRegion(Region $region)
	{
		$response = $this->client->request('GET', self::SEARCH_URL, [
			'query' => [
				'latitude' => $region->getLat(),
				'longitude' => $region->getLng()
			]
		]);

		return $this->parseResponse(json_decode((string) $response->getBody()));
	}

	private function parseResponse($data)
	{
		$mappedData = array_map(function($item){
			return [
				'company' => $item->displayName,
				'address_1' => $item->address->line1,
				'address_2' => $item->address->line2,
				'town' => $item->address->town,
				'county' => $item->address->region,
				'postcode' => $item->address->postalCode,
				'email' => $item->address->email,
				'telephone' => $item->address->phone,
				'fax' => $item->address->fax,
				'source' => self::SOURCE_NAME
			];
		}, $data->results);
		return $mappedData;
	}
}