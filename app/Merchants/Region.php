<?php

namespace App\Merchants;

class Region
{
	private $code;
	private $postcode;
	private $lat;
	private $lng;

	public function __construct(array $data)
	{
		$this->code = $data['code'];
		$this->lat = $data['lat'];
		$this->lng = $data['lng'];
		if(isset($data['postcode'])) {
			$this->postcode = $data['postcode'];
		}
	}

	public function getCode()
	{
		return $this->code;
	}

	public function getPostcode()
	{
		return $this->postcode;
	}

	public function getLat()
	{
		return $this->lat;
	}

	public function getLng()
	{
		return $this->lng;
	}
}