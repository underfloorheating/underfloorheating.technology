<?php

namespace App\Merchants;

use Illuminate\Database\Eloquent\Model;

class Merchant extends Model
{
	protected $fillable = [
		'company',
		'address_1',
		'address_2',
		'address_3',
		'town',
		'county',
		'postcode',
		'telephone',
		'fax',
		'email',
		'contact',
		'longitude',
		'latitude',
		'source'
	];
}
