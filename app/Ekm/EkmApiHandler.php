<?php

namespace App\Ekm;

use Carbon\Carbon;
use GuzzleHttp\Client;

class EkmApiHandler
{
    public function init($apiKey)
    {
    	$this->client = new Client([
    		'base_uri' => config('ekm.api.endpoint'),
    		'headers' => [
    			'Authorization' => 'Bearer '.$apiKey
    		]
    	]);
    }

    public function getOrder($id)
    {
    	$response = $this->client->request('GET', "orders/$id");
    	$body = json_decode((string) $response->getBody());
    	return $body->data;
    }

    public function getOrders()
    {
    	$data = [];
    	$pageNumber = 1;
    	do {
	    	$response = $this->client->request('GET', 'orders/search', [
	    		'query' => [
	    			'page' => $pageNumber,
	    			'limit' => 20,
	    			'query' => "Status eq 'Pending' and is_confirmed eq true",
	    			'orderby' => '-OrderDate'
	    		]
	    	]);
	    	$pageNumber++;
		    $body = json_decode((string) $response->getBody());
		    $data = array_merge($data, $body->data);
		} while ($pageNumber <= $body->meta->pages_total);

	    return $data;
	}

	public function setOrderToComplete($orderNumber)
	{
		$response = $this->client->request('PUT', "orders/$orderNumber/status", [
			'http_errors' => false,
			'json' => [
				'status' => 'complete',
				'email_customer' => false
			]
		]);
		switch ($response->getStatusCode()) {
			case 200:
				return "order id $orderNumber - status changed to 'complete'";
			case 400:
				throw new \Exception("order id $orderNumber - an invalid request was made while trying to update status to complete");
			case 401:
				throw new \Exception("order id $orderNumber - invalid bearer token while trying to update status to complete");
			case 403:
				throw new \Exception("order id $orderNumber - not allowed to update status");
			case 404:
				throw new \Exception("No order with id $orderNumber was found");
			default:
				throw new \Exception("order id $orderNumber - an unknown error occured " . $response->getStatusCode() . ' '  . $response->getReasonPhrase());

		}
	}

	public function getOutOfStockProducts()
	{
		// Get OOS variants
		$response = $this->client->request('GET', "variants/search", [
			'query' => [
				'query' => 'number_in_stock eq 0'
			]
		]);
		$body = json_decode((string) $response->getBody());
		$data = collect($body->data)->map(function($item){
			return $item->product_code;
		});

		// Get OOS parents
		$response = $this->client->request('GET', "products/search", [
			'query' => [
				'query' => 'number_in_stock eq 0'
			]
		]);
		$body = json_decode((string) $response->getBody());
		$data = $data->merge(collect($body->data)->map(function($item){
			return $item->product_code;
		}))->unique();

	    return $data;
	}
}
