<?php

namespace App\Ekm;

class EkmXmlBuilder
{
	private $dom;
	private $root;
	private $orders;

	private $paymentTypes;

	public function __construct($site)
	{
		$this->site = $site;
		$this->paymentTypes = config('ekm.payment_types');

		$this->dom = new \domDocument("1.0", "UTF-8");
		$this->root = $this->dom->createElement("XMLFile");
		$this->root->setAttributeNS('http://www.w3.org/2000/xmlns/', 'xmlns:xsd', 'http://www.w3.org/2001/XMLSchema');
		$this->root->setAttributeNS('http://www.w3.org/2000/xmlns/', 'xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
		$this->root = $this->dom->appendChild($this->root);
		$this->orders = $this->dom->createElement("SalesOrders");
		$this->root->appendChild($this->orders);
	}

	public function asXml()
	{
		return $this->dom->saveXML();
	}

	public function __toString()
	{
		return $this->asXml();
	}

	public function addOrder($orderDetails) {
	    # Create the order node
		$order = $this->dom->createElement("SalesOrder");
		$this->orders->appendChild($order);

		# Add the order details to the order node
		$this->addElement($order, "OrderNumber", $this->site->getAccountNumberPrefix() . $orderDetails->id);
		$this->addElement($order, "CustomerOrderRef", $orderDetails->order_number);
		$this->addElement($order, "OrderDate", date("Y-m-d\TH:i:s", strtotime($orderDetails->order_date)));

		# Add the customer node to the order
		$customer = $this->addCustomer($order, $orderDetails);

		# Add the customer contact node to the order
		$this->addCustomerContact($customer, $orderDetails);

		# Add the delivery address node to the customer
		$this->addDeliveryAddress($customer, $orderDetails);

		# Order financials, delivery and special instructions
		$this->addDeliveryCosts($order, $orderDetails);

		# Add the payment nodes
		$this->addPayment($order, $orderDetails);

		# Add any Discount nodes
		$this->addDiscount($order, $orderDetails);

		# Create the product nodes
		$this->addProducts($order, $orderDetails);
	}

	/**
	 * Generates an OrderWise account number depending on the whether this
	 * was a registered customer or a guest checkout
	 *
	 * @param  obj 	$orderDetails 	The details we get back from the API call
	 * @return string             	The new orderwise account number
	 */
	private function getAccountNumber($orderDetails)
	{
		if($orderDetails->customer_details->customer_id != 0) { // Registered customer
			return $this->site->getAccountNumberPrefix() . $orderDetails->customer_details->customer_id;
		} else {												// Guest checkout
			return $this->site->getAccountNumberPrefixForGuest() . $this->site->getLastAccountId();
		}
	}

	private function addCustomer(&$order, $orderDetails)
	{
		$customer = $this->dom->createElement("Customer");
		$order->appendChild($customer);

		$accountNumber = $this->getAccountNumber($orderDetails);

		$this->addElement($customer, "AccountNumber", $accountNumber);
		$this->addElement($customer, "eCommerceAccountNumber", $accountNumber);
		$this->addElement($customer, "InvoiceName", $this->getInvoiceName($orderDetails));
		$this->addElement($customer, "InvoiceAddress1", $orderDetails->customer_details->address);
		$this->addElement($customer, "InvoiceAddress2", $orderDetails->customer_details->address2);
		$this->addElement($customer, "InvoiceTown", $orderDetails->customer_details->town);
		$this->addElement($customer, "InvoiceCounty", $orderDetails->customer_details->county);
		$this->addElement($customer, "InvoiceCountry", $orderDetails->customer_details->country);
		$this->addElement($customer, "InvoiceCountryCode", $orderDetails->customer_details->country);
		$this->addElement($customer, "InvoicePostcode", $orderDetails->customer_details->post_code);
		$this->addElement($customer, "InvoiceEmail", $orderDetails->customer_details->email_address);
		$this->addElement($customer, "InvoiceTelephone", $orderDetails->customer_details->telephone);
		$this->addElement($customer, "InvoiceFax", $orderDetails->customer_details->fax);
		$this->addElement($customer, "StatementName", $this->getInvoiceName($orderDetails));
		$this->addElement($customer, "StatementAddress1", $orderDetails->customer_details->address);
		$this->addElement($customer, "StatementAddress2", $orderDetails->customer_details->address2);
		$this->addElement($customer, "StatementTown", $orderDetails->customer_details->town);
		$this->addElement($customer, "StatementCounty", $orderDetails->customer_details->county);
		$this->addElement($customer, "StatementCountryCode", $orderDetails->customer_details->country);
		$this->addElement($customer, "StatementPostcode", $orderDetails->customer_details->post_code);
		$this->addElement($customer, "StatementEmail", $orderDetails->customer_details->email_address);
		$this->addElement($customer, "StatementTelephone", $orderDetails->customer_details->telephone);
		$this->addElement($customer, "StatementFax", $orderDetails->customer_details->fax);

		return $customer;
	}

	private function addCustomerContact(&$customer, $orderDetails)
	{
		$contact = $this->dom->createElement("CustomerContact");
		$customer->appendChild($contact);
		$this->addElement($contact, "Name", $orderDetails->customer_details->first_name.' '.$orderDetails->customer_details->last_name);
		$this->addElement($contact, "Telephone", $orderDetails->customer_details->telephone);
		$this->addElement($contact, "Fax", $orderDetails->customer_details->fax);
		$this->addElement($contact, "Email", $orderDetails->customer_details->email_address);
	}

	private function addDeliveryAddress(&$customer, $orderDetails)
	{
		$data = $orderDetails->shipping_address != null ? $orderDetails->shipping_address : $orderDetails->customer_details ;
		$deliveryAddress = $this->dom->createElement("DeliveryAddress");
		$customer->appendChild($deliveryAddress);
		$this->addElement($deliveryAddress, "Name", $this->getDeliveryName($orderDetails));
		$this->addElement($deliveryAddress, "Contact", $data->first_name.' '.$data->last_name);
		$this->addElement($deliveryAddress, "Address1", $data->address);
		$this->addElement($deliveryAddress, "Address2", $data->address2);
		$this->addElement($deliveryAddress, "Town", $data->town);
		$this->addElement($deliveryAddress, "County", $data->county);
		$this->addElement($deliveryAddress, "CountryCode", $data->country);
		$this->addElement($deliveryAddress, "Postcode", $data->post_code);
		$this->addElement($deliveryAddress, "Email", $orderDetails->customer_details->email_address);
		$this->addElement($deliveryAddress, "Telephone", $data->telephone);
		$this->addElement($deliveryAddress, "Fax", $data->fax);
	}

	private function addDeliveryCosts(&$order, $orderDetails)
	{
		$deliveryTax = $orderDetails->total_delivery * ($orderDetails->delivery_tax_rate / 100);
		$deliveryNet = $orderDetails->total_delivery - $deliveryTax;

		$this->addElement($order, "DeliveryMethod", $orderDetails->delivery_method);
		$this->addElement($order, "DeliveryNet", number_format($deliveryNet, 2, '.', ''));
		$this->addElement($order, "DeliveryTax", number_format($deliveryTax, 2, '.', ''));
		$this->addElement($order, "DeliveryGross", number_format($orderDetails->total_delivery, 2, '.', ''));
		$this->addElement($order, "DeliveryTaxCode", 'T1');

		$this->addElement($order, "OrderNet", number_format($orderDetails->sub_total, 2, '.', ''));
		$this->addElement($order, "OrderTax", number_format($orderDetails->total_tax, 2, '.', ''));
		$this->addElement($order, "OrderGross", number_format($orderDetails->total_cost, 2, '.', ''));
		$this->addElement($order, "AmountPaid", number_format($orderDetails->total_cost, 2, '.', ''));
	}

	private function addPayment(&$order, $orderDetails)
	{
		$payments = $this->dom->createElement("Payments");
		$order->appendChild($payments);
		$salesPayment = $this->dom->createElement("SalesPayment");
		$payments->appendChild($salesPayment);
		$this->addElement($salesPayment, "Description", $this->translatePaymentType($orderDetails->order_type));
		$this->addElement($salesPayment, "Amount", number_format($orderDetails->total_cost, 2, '.', ''));
	}

	private function addDiscount(&$order, $orderDetails)
	{
		if($orderDetails->discounts == NULL) {
			return false;
		}
		$discountName = strip_tags(preg_replace('/\[\/?DISCOUNT\]/', '', $orderDetails->discounts));
		$disSurs = $this->dom->createElement("Dissurs");
		$order->appendChild($disSurs);
		$salesDisSur = $this->dom->createElement("SalesDissur");
		$disSurs->appendChild($salesDisSur);
		$this->addElement($salesDisSur,"Description",$discountName);
		$this->addElement($salesDisSur,"Price",number_format((float)$orderDetails->discounts_total,4,".",""));
		$this->addElement($salesDisSur,"TaxCode",'T1');
		$this->addElement($salesDisSur,"GrossDiscount",'true');

	}

	private function translatePaymentType($type)
	{
		return array_key_exists($type, $this->paymentTypes) ? $this->paymentTypes[$type] : $type ;
	}

	private function addProducts(&$order, $orderDetails)
	{
		$lines = $this->dom->createElement("Lines");
		$order->appendChild($lines);
		foreach ($orderDetails->items as $item) {
			if($item->product == null){
				continue;
			}
			$line = $this->dom->createElement("SalesOrderLine");
			$lines->appendChild($line);
			$this->addElement($line, "eCommerceCode", $item->product->product_code);
			$this->addElement($line, "Code", $item->product->product_code);
			$this->addElement($line, "Quantity", $item->quantity);
			$this->addElement($line, "eCommerceItemID", $item->id);

			$this->addElement($line, "ItemGross", number_format($item->item_price, 2, '.', ''));
			$this->addElement($line, "TaxCode", "T1");
		}
	}

	private function getInvoiceName($orderDetails)
	{
		return $orderDetails->customer_details->company != ''
			   ? $orderDetails->customer_details->company
			   : $orderDetails->customer_details->first_name.' '.$orderDetails->customer_details->last_name;
	}

	private function getDeliveryName($orderDetails)
	{
		if($orderDetails->shipping_address == null) {
			return $this->getInvoiceName($orderDetails);
		}

		return $orderDetails->shipping_address->company != ''
			   ? $orderDetails->shipping_address->company
			   : $orderDetails->shipping_address->first_name.' '.$orderDetails->shipping_address->last_name;
	}

	private function addElement(&$parent, $node, $value = "")
	{
		$value = utf8_encode(trim($value));
		if ($value == "") return;

		if (preg_match("/[^ 0-9a-z\-\/\"\.\@\+\'\:\, \(\)]/i", $value)) {
			$e = $this->dom->createElement($node);
			$e->appendChild($this->dom->createCDATASection($value));
			$parent->appendChild($e);
		}
		else {
			$parent->appendChild($this->dom->createElement($node, $value));
		}
	}
}
