<?php

namespace App\Ekm;

use Illuminate\Database\Eloquent\Model;

class EkmOrder extends Model
{
    protected $fillable = ['order_number'];
}
