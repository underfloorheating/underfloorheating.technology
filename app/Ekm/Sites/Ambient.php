<?php

namespace App\Ekm\Sites;

use App\Ekm\EkmApiHandler;
use App\Ekm\Sites\AbstractSite;
use App\Ekm\Sites\SiteInterface;

class Ambient extends AbstractSite implements SiteInterface
{
	private const PREFIX = 'ambekm-';

    public function getApiKey()
    {
    	return config('ekm.api.keys.ambient.'.session('api_application'));
    }

    public function getAccountNumberPrefix()
    {
    	return self::PREFIX;
    }
}
