<?php

namespace App\Ekm\Sites;

use App\Ekm\EkmApiHandler;
use App\Ekm\EkmOrder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

abstract class AbstractSite
{
	const TABLE_LAST_ACCOUNT_ID = 'last_account_id';

    public function __construct(EkmApiHandler $apiHandler)
	{
		$this->api = $apiHandler;
		$this->api->init($this->getApiKey());
	}

	public function getOrders($pageNum)
    {
    	return $this->api->getOrders();
    }

    public function getOrder($id)
    {
    	return $this->api->getOrder($id);
    }

    private function getSite()
    {
    	return strtolower(substr(static::class, strrpos(static::class, '\\') + 1));
    }

    public function getLastAccountId()
    {
    	$accountIdObj = DB::table(self::TABLE_LAST_ACCOUNT_ID)
    		->where('classname', static::class)
    		->limit(1)
    		->first();
    	if(empty($accountIdObj)) {
    		$newId = 1;
    		DB::table(self::TABLE_LAST_ACCOUNT_ID)->insert([
    			'classname' => static::class,
    			'last_account_id' => $newId,
    			'created_at' => now()->toDateTimeLocalString(),
    			'updated_at' => now()->toDateTimeLocalString()
    		]);
    	} else {
    		$newId = $accountIdObj->last_account_id + 1;
    		DB::table(self::TABLE_LAST_ACCOUNT_ID)
    			->where('classname', static::class)
    			->update(['last_account_id' => $newId]);
    	}
    	return $newId;
    }

    public function getAccountNumberPrefixForGuest()
    {
    	return $this->getAccountNumberPrefix() . 'g-';
    }

    public function setOrderToComplete($orderNumber)
    {
    	return $this->api->setOrderToComplete($orderNumber);
    }

    public function getOutOfStockProducts()
    {
    	return $this->api->getOutOfStockProducts();
    }
}
