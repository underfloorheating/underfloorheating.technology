<?php

namespace App\Ekm\Sites;

use App\Ekm\EkmApiHandler;

interface SiteInterface
{
	public function __construct(EkmApiHandler $apiHandler);
	public function getApiKey();
	public function getAccountNumberPrefix();
}