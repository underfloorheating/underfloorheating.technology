<?php

namespace App\Ekm\Sites;

use App\Ekm\EkmApiHandler;
use App\Ekm\Sites\AbstractSite;
use App\Ekm\Sites\SiteInterface;

class Prowarm extends AbstractSite implements SiteInterface
{
	private const PREFIX = 'pwshop-';

    public function getApiKey()
    {
    	return config('ekm.api.keys.prowarm');
    }

    public function getAccountNumberPrefix()
    {
    	return self::PREFIX;
    }
}
