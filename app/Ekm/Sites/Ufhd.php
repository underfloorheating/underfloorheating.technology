<?php

namespace App\Ekm\Sites;

use App\Ekm\EkmApiHandler;
use App\Ekm\Sites\AbstractSite;
use App\Ekm\Sites\SiteInterface;

class Ufhd extends AbstractSite implements SiteInterface
{
	private const PREFIX = 'ufhd-';

    public function getApiKey()
    {
    	return config('ekm.api.keys.ufhd.'.session('api_application'));
    }

    public function getAccountNumberPrefix()
    {
    	return self::PREFIX;
    }
}
