<?php

namespace App\Ekm\Sites;

use App\Ekm\EkmApiHandler;

class SiteFactory
{
    public static function create($siteName)
    {
    	$sitePath = 'Ekm/Sites/'.ucfirst($siteName);
    	if(!file_exists(app_path($sitePath).'.php')) {
			throw new \Exception('No site exists with the name ' . $siteName);
    	}
    	// Convert file path to instantiable class path
    	$sitePath = 'App\\' . preg_replace('/\//', '\\', $sitePath);
    	return new $sitePath(new EkmApiHandler);
    }
}
