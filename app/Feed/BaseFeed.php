<?php

namespace App\Feed;

use App\Feed\FeedExclusion;
use App\Feed\FeedItem;
use DB;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use League\Csv\Writer;

abstract class BaseFeed
{
	const DB_TABLE_FEED = 'feed_items';
	const DB_TABLE_REWRITES = 'feed_rewrites';
	const TEMP_FILE = 'uploads/feed.csv';

	private $feedData;

	// --------------------------------//
	//          PUBLIC METHODS         //
	// --------------------------------//

	public function __construct()
	{
		$typeName = str_replace(__NAMESPACE__.'\\', '', get_class($this));
		// Use the name of the calling class to create the feedType variable value
		$this->feedType = strtolower(str_replace('Feed', '', $typeName));
	}

	/**
	 * Save the feed data to the database
	 */
	final public function saveFeedData()
	{
		// Download the feed data
		$this->downloadFeedData();
		// Get the data into a usable array format
		$this->parseFeedData();
		// Delete all existing database entries for this feed
		DB::table(self::DB_TABLE_FEED)->where('feed_type', $this->feedType)->delete();
		// Add each newly downloaded row to the database
		foreach($this->feedData as $row) {
			FeedItem::create($row);
		}
	}

	/**
	 * Create a csv formatted data stream for the current Feeds data
	 *
	 * @param  string 	$delimiter 	The delimiter we want to use for the feed
	 * @return string            	The CSV formatted string
	 */
	final public function getFeedData($delimiter = "\t")
	{
		$this->loadFeedData();
		return $this->createCsv();
	}

	// ---------------------------------//
	//          PRIVATE METHODS         //
	// ---------------------------------//

	/**
	 * Downloads and savess the feed data against this object
	 */
	private function downloadFeedData()
	{
		try {
			$client = new Client();
			$res = $client->request('GET', static::FEED_URL);
			$this->feedData = (string) $res->getBody();
		} catch (ClientException $e) {
			throw new \Exception('Failed to download '.static::FEED_URL.' feed.  Received error '.$e->getResponse()->getStatusCode().' - '.$e->getResponse()->getReasonPhrase());
		}
	}

	private function parseFeedData()
	{
		// Create a BOM we can use to remove one from the file
		$bom = pack('H*', 'EFBBBF');

		// Split the feed data into a usable array
		$feedData = array_map(function($line) use ($bom){
			return str_getcsv(preg_replace("/^$bom/", '', $line), "\t");
		}, explode(static::DOWNLOAD_DELIMITER, $this->feedData));

		$feedData = array_filter($feedData, function($row) use ($feedData) {
			return count($row) == count($feedData[0]);
		});
		// Convert each row to an associative array
		$feedData = array_map(function($row) use ($feedData) {
			return $this->adjustSuppliedData(
				array_combine(
					$feedData[0],
					array_map('trim', $row)
				)
			);
		}, $feedData);

		// Remove any that were set to false during the above array_map
		$feedData = array_filter($feedData, function($row) {
			return $row;
		});
		// Remove the initial column headers row
		array_shift($feedData);
		$this->feedData = $feedData;
	}

	/**
	 * Return feedData content in CSV format
	 *
	 * @return string CSV representation of feedData variable content
	 */
	private function createCsv()
	{
		$csv = Writer::createFromFileObject(new \SplTempFileObject());
		$csv->setDelimiter("\t");

		$csv->insertOne(array_keys($this->feedData[0]));
		foreach($this->feedData as $item) {
			$csv->insertOne($item);
		}
		return $csv->getContent();
	}

	/**
	 * Loads feed data for the current feed from the database
	 */
	private function loadFeedData()
	{
		// Get all exclusions for current feed type
		$exclusions = FeedExclusion::where('feed_type', $this->feedType);

		$feedItems = FeedItem::where('feed_type', $this->feedType)
							 ->whereNotIn('ecomm_id', $exclusions->pluck('ecomm_id'))
							 ->withCount('rewrites')
							 ->get();
		if(!$feedItems) {
			throw new \Exception('Couldn\'t find any feed data for '.$this->feedType);
		}
		$this->feedData = $feedItems->map(function($item, $key){
			$rewriteData = [];
			if($item->rewrites_count > 0) {
				$rewriteData = $item->rewrites()->first()->toArray();
			}
			$item = $item->toArray();
			foreach($rewriteData as $key => $value) {
				if($value !== NULL && $key != 'id') {
					$item[$key] = $value;
				}
			}
			$item['price'] = $item['price'].' '.$item['currency'];
			$item['id'] = $item['ecomm_id'];
			// Remove local DB fields we don't want to send to GMC
			unset($item['ecomm_id']);
			unset($item['feed_type']);
			unset($item['rewrites_count']);
			unset($item['created_at']);
			unset($item['updated_at']);
			unset($item['feed_type']);
			return $item;
		});
	}
}