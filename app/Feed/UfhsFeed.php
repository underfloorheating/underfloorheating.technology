<?php

namespace App\Feed;

class UfhsFeed extends BaseFeed
{
	const FEED_URL = 'https://www.theunderfloorheatingstore.com/media/feeds/feed_11.txt';
	const DOWNLOAD_DELIMITER = "\n";

	private $skuPatterns;

	public function __construct()
	{
		parent::__construct();
		$this->skuPatterns = config('feed.sku-patterns');
	}

	/**
	 * Rewrite any data we are given from the feed suppliers.  This function
	 * will receive a single row from the feed data at a time.
	 *
	 * @param  array 	$data 	A single row of the feed data
	 * @return array 			The amended array
	 */
	protected function adjustSuppliedData($data)
	{
		// Check if we have a bundle child sku.
		$currentSku = $data['mpn'];
		$foundSku = array_filter($this->skuPatterns, function($pattern) use ($currentSku) {
			return preg_match($pattern,$currentSku);
		});
		// If we do, respond with a signal to delete the row.
		if(!empty($foundSku)) {
			return false;
		}
		$data['feed_type'] = $this->feedType;
		$data['ecomm_id'] = $data['id'];
		// Remove all size options apart from the first one
		$sizes = explode(',', $data['size']);
		if(count($sizes) > 1) {
			$data['size'] = array_shift($sizes);
		}
		unset($data['id']);
		// Get rid of any blank fields so the DB defaults are used as expected
		foreach($data as $key=>$value) {
			if(empty($value)) {
				unset($data[$key]);
			}
		}
		return $data;
	}
}