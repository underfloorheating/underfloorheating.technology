<?php

namespace App\Feed;

abstract class EkmBaseFeed extends BaseFeed
{
	const DOWNLOAD_DELIMITER = "\r\n";

	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Rewrite any data we are given from the feed suppliers.  This function
	 * will receive a single row from the feed data at a time.
	 *
	 * @param  array 	$data 	A single row of the feed data
	 * @return array 			The amended array
	 */
	protected function adjustSuppliedData($data)
	{
		$data['feed_type'] = $this->feedType;
		$data['custom_label_1'] = $data['title'];
		$data['ecomm_id'] = $data['id'];
		$data['shipping'] = '';
		$data = array_map('trim', $data);
		unset($data['id']);
		// Get rid of any blank fields so the DB defaults are used as expected
		foreach($data as $key=>$value) {
			if(empty($value)) {
				unset($data[$key]);
			}
		}
		return $data;
	}
}