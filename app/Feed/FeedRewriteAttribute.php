<?php

namespace App\Feed;

use Awobaz\Compoships\Database\Eloquent\Model;

class FeedRewriteAttribute extends Model
{
    protected $fillable = [
		'ecomm_id',
		'feed_type',
		'title',
		'link',
		'price',
		'description',
		'condition',
		'shipping',
		'shipping_weight',
		'shipping_label',
		'gtin',
		'brand',
		'mpn',
		'identifier_exists',
		'item_group_id',
		'color',
		'size',
		'image_link',
		'additional_image_link',
		'product_type',
		'google_product_category',
		'quantity',
		'availability',
		'sale_price_effective_date',
		'sale_price',
		'currency',
		'custom_label_0',
		'custom_label_1',
		'custom_label_2',
		'custom_label_3',
		'custom_label_4',
		'is_bundle',
    ];
}
