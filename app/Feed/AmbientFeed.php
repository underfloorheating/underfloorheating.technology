<?php

namespace App\Feed;

class AmbientFeed extends EkmBaseFeed
{
	const FEED_URL = 'https://youraccount.30.ekm.net/ekmps/shops/995665/data/ekm_p_995665.txt';

	public function __construct()
	{
		parent::__construct();
	}
}