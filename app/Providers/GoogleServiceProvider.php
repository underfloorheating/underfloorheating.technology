<?php

namespace App\Providers;

use Google_Client;
use Google_Service_Sheets;

use Illuminate\Support\ServiceProvider;

class GoogleServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
    	$this->app->singleton('google_spreadsheets_client', function ($app) {
			// we will instantiate the Google Spread Sheet Client once in this function
    		$client = new Google_Client();
    		$client->setApplicationName('Google Sheets API PHP Quickstart');
    		$client->setScopes(Google_Service_Sheets::SPREADSHEETS_READONLY);
    		$client->setAuthConfig(base_path() . '/discount-my-quote-google-sheets-credentials.json');
    		$client->setAccessType('offline');
    		$client->setPrompt('select_account consent');

    		$service = new Google_Service_Sheets($client);
    		return $service;
    	});
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
