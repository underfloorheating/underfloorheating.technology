<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use League\Csv\Writer;
use Sunra\PhpSimple\HtmlDomParser;

class DownloadPoliticianEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $location;
    private $name;
    private $url;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($location, $name, $url)
    {
        $this->location = $location;
        $this->name = $name;
        $this->url = $url;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $personHtml = HtmlDomParser::str_get_html(file_get_contents($this->url));
		$emailLink = $personHtml->find('a[href*="mailto"]', 0);
		if($emailLink) {
			$writer = Writer::createFromPath(storage_path().'/app/politicians.csv', 'a');
			$writer->insertOne([
				'location' => $this->location,
				'name'=> $this->name,
				'email' => str_replace('mailto:', '', $emailLink->href)
			]);
		}
    }
}
