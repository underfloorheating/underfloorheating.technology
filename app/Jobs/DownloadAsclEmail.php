<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use League\Csv\Writer;
use Sunra\PhpSimple\HtmlDomParser;

class DownloadAsclEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $url;
    private $region;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($url, $region)
    {
        $this->url = $url;
        $this->region = $region;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
		$writer = Writer::createFromPath(storage_path().'/app/ascl.csv', 'a');
        $personHtml = HtmlDomParser::str_get_html(file_get_contents($this->url));
		foreach($personHtml->find('table tr') as $row) {
			$writer->insertOne([
				'region' => $this->region,
				'location' => $row->find('td', 0)->plaintext,
				'name'=> $row->find('td', 1)->plaintext . ' ' . $row->find('td', 2)->plaintext,
				'email' => $row->find('td', 3)->plaintext
			]);
		}
    }
}
