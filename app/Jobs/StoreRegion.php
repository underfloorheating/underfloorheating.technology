<?php

namespace App\Jobs;

use App\Merchants\Merchant;
use App\Merchants\Region;
use App\Merchants\Sources\SourceInterface;
use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class StoreRegion implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $sourceName;
    private $region;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 5;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($sourceName, $region)
    {
        $this->sourceName = $sourceName;
        $this->region = $region;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
    	$sourceName = $this->sourceName;
    	$source = new $sourceName(new Client);

        foreach($source->getDataForRegion(new Region($this->region)) as $details) {
			Merchant::firstOrCreate($details);
		}
    }
}