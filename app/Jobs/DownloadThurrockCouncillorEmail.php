<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use League\Csv\Writer;
use Sunra\PhpSimple\HtmlDomParser;

class DownloadThurrockCouncillorEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $url;
    private $name;
    private $area;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($url, $name, $area)
    {
        $this->url = $url;
        $this->name = $name;
        $this->area = $area;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $personHtml = HtmlDomParser::str_get_html(file_get_contents($this->url));
		$emailLink = $personHtml->find('.mgUserBody a[href*="mailto"]', 0);
		if($emailLink) {
			$writer = Writer::createFromPath(storage_path().'/app/thurrock.csv', 'a');
			$writer->insertOne([
				'location' => $this->area,
				'name'=> $this->name,
				'email' => str_replace('mailto:', '', $emailLink->href)
			]);
		}
    }
}
