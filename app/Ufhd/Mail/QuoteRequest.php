<?php

namespace App\Ufhd\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Ufhd\Quote;

class QuoteRequest extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The Quote instance
     *
     * @var Quote
     */
    public $quote;

    /**
     * An array of Quotefile objects
     *
     * @var Array
     */
    public $files;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Quote $quote, array $files)
    {
        $this->quote = $quote;
        $this->files = $files;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('no-reply@underfloorheating-direct.com')
        	->subject('A quote has been requested through UFHD')
        	->view('ufhd.request-a-quote.submission-email');
    }
}
