<?php

namespace App\Ufhd;

use Illuminate\Database\Eloquent\Model;

class Quote extends Model
{
    protected $fillable = [
		'first_name',
		'last_name',
		'email',
		'telephone',
		'company_name',
		'vat_number',
		'add1',
		'add2',
		'town',
		'county',
		'postcode',
		'country',
		'details',
		'system_type'
	];

	public function quotefiles() {
		return $this->hasMany(Quotefile::class);
	}
}

