<?php

namespace App\Ufhd;

use Illuminate\Database\Eloquent\Model;

class Quotefile extends Model
{
    protected $fillable = [
		'file_name',
		'file_ext',
		'file_type',
		'upload_name',
		'quote_id'
    ];

    public function quote() {
    	return $this->belongsTo(Quote::class);
    }
}

