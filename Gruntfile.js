module.exports = function(grunt) {

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		concat: {
			ufhd: {
				src: ['resources/ufhd/js/app.js'],
				dest: 'public/ufhd/js/app.js'
			},
			ufhd_vendor_js: {
				src: [
					'node_modules/jquery/dist/jquery.min.js',
					'node_modules/bootstrap/dist/js/bootstrap.min.js'
				],
				dest: 'public/ufhd/js/vendor.js'
			}
		},
		sass: {
			ufhd: {
				options: {
					style: 'expanded',
					sourcemap: 'file'
				},
				files: [{
					expand: true,
					cwd: 'resources/ufhd/sass',
					src: ['*.scss'],
					dest: 'public/ufhd/css',
					ext: '.css'
				}]
			}
		},
		watch: {
			ufhd: {
				options: {
					livereload: true
				},
				files: 'resources/ufhd/**/*.*',
				tasks: ['sass:ufhd', 'concat:ufhd', 'concat:ufhd_vendor_js']
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-concat');

	grunt.registerTask('default', ['sass']);
	grunt.registerTask('ufhd', ['sass:ufhd', 'concat:ufhd', 'concat:ufhd_vendor_js']);

};