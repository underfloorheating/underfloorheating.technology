<?php

use Illuminate\Database\Seeder;

class StoresSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('stores')->insert([
        	[
        		'label' => 'UFHS eBay',
        		'store_name' => 'underfloorheatingstore',
        		'platform_id' => 1
        	],
        ]);
    }
}
