<?php

use Illuminate\Database\Seeder;

class PlatformSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('platforms')->insert([
        	[
        		'id' => 1,
        		'name' => 'ebay'
        	],
        	[
        		'id' => 2,
        		'name' => 'amazon'
        	]
        ]);
    }
}
