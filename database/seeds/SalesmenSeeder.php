<?php

use Illuminate\Database\Seeder;

class SalesmenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('dmq_salesmen')->insert([
    		[
    			'username' => 'Aaron',
    			'email' => 'aaron@theunderfloorheatingstore.com',
    			'first_name' => 'Aaron',
    			'last_name' => 'Lewis',
    			'telephone' => '01268 663081'
    		],
    		[
    			'username' => 'Andy',
    			'email' => 'andrew@theunderfloorheatingstore.com',
    			'first_name' => 'Andrew',
    			'last_name' => 'Fryer',
    			'telephone' => '01268 663080'
    		],
    		[
    			'username' => 'Ben',
    			'email' => 'ben@theunderfloorheatingstore.com',
    			'first_name' => 'Ben',
    			'last_name' => 'Fisher',
    			'telephone' => '01268 663082'
    		],
    		[
    			'username' => 'Brad',
    			'email' => 'brad@theunderfloorheatingstore.com',
    			'first_name' => 'Bradley',
    			'last_name' => 'Howard-Simpson',
    			'telephone' => '01268 663088'
    		],
    		[
    			'username' => 'DanC',
    			'email' => 'dan.c@theunderfloorheatingstore.com',
    			'first_name' => 'Daniel',
    			'last_name' => 'Cooper',
    			'telephone' => '01268 663098'
    		],
    		[
    			'username' => 'David P',
    			'email' => 'david.prendergast@theunderfloorheatingstore.com',
    			'first_name' => 'David',
    			'last_name' => 'Prendergast',
    			'telephone' => '01268 663090'
    		],
    		[
    			'username' => 'Dean',
    			'email' => 'dean@theunderfloorheatingstore.com',
    			'first_name' => 'Dean',
    			'last_name' => 'Shaw',
    			'telephone' => '01268 662428'
    		],
    		[
    			'username' => 'Jake',
    			'email' => 'jake@theunderfloorheatingstore.com',
    			'first_name' => 'Jake',
    			'last_name' => 'Martin',
    			'telephone' => '01268 663083'
    		],
    		[
    			'username' => 'James',
    			'email' => 'james@theunderfloorheatingstore.com',
    			'first_name' => 'James',
    			'last_name' => 'Parmenter',
    			'telephone' => '01268 744165'
    		],
    		[
    			'username' => 'Josh',
    			'email' => 'josh@theunderfloorheatingstore.com',
    			'first_name' => 'Josh',
    			'last_name' => 'Charles',
    			'telephone' => '01268 663093'
    		],
    		[
    			'username' => 'Lee',
    			'email' => 'lee@theunderfloorheatingstore.com',
    			'first_name' => 'Lee',
    			'last_name' => 'Aylott',
    			'telephone' => '01268 663087'
    		],
    		[
    			'username' => 'Luke',
    			'email' => 'luke@theunderfloorheatingstore.com',
    			'first_name' => 'Luke',
    			'last_name' => 'Warbey',
    			'telephone' => '01268 645617'
    		],
    		[
    			'username' => 'Ryan',
    			'email' => 'ryan@theunderfloorheatingstore.com',
    			'first_name' => 'Ryan',
    			'last_name' => 'Skinner',
    			'telephone' => '01268 663091'
    		],
    		[
    			'username' => 'Sam',
    			'email' => 'sam@theunderfloorheatingstore.com',
    			'first_name' => 'Sam',
    			'last_name' => 'Stevens',
    			'telephone' => '01268 662420'
    		],
    		[
    			'username' => 'Scott',
    			'email' => 'scott@theunderfloorheatingstore.com',
    			'first_name' => 'Sam',
    			'last_name' => 'Yule',
    			'telephone' => '01268 663099'
    		]
    	]);
    }
}