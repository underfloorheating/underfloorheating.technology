<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiscountMyQuoteQuotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dmq_quotes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('salesman_username')->nullable();
            $table->string('order_number');
            $table->string('customer_order_ref')->nullable();
            $table->string('order_date');
            $table->unsignedDecimal('order_net', 7, 2);
            $table->string('name');
            $table->string('email');
            $table->unsignedDecimal('discount_applied', 4, 2);
            $table->unsignedDecimal('available_discount', 4, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dmq_quotes');
    }
}
