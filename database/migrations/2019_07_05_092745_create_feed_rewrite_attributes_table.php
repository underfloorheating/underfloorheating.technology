<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeedRewriteAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feed_rewrite_attributes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ecomm_id', 100);
			$table->string('feed_type', 20);
			$table->string('title', 255)->nullable();
			$table->string('link', 255)->nullable();
			$table->decimal('price', 6,2)->nullable();
			$table->text('description')->nullable();
			$table->string('condition', 10)->nullable();
			$table->string('shipping', 255)->nullable();
			$table->string('shipping_weight', 20)->nullable();
			$table->string('shipping_label', 30)->nullable();
			$table->string('gtin', 20)->nullable();
			$table->string('brand', 50)->nullable();
			$table->string('mpn', 100)->nullable();
			$table->string('identifier_exists', 3)->nullable();
			$table->string('item_group_id', 60)->nullable();
			$table->string('color', 30)->nullable();
			$table->string('size', 255)->nullable();
			$table->string('image_link', 255)->nullable();
			$table->text('additional_image_link')->nullable();
			$table->string('product_type', 255)->nullable();
			$table->string('google_product_category', 255)->nullable();
			$table->unsignedMediumInteger('quantity')->nullable();
			$table->string('availability', 30)->nullable();
			$table->datetime('sale_price_effective_date')->nullable();
			$table->string('sale_price', 30)->nullable();
			$table->string('currency', 3)->nullable();
			$table->string('custom_label_0', 255)->nullable();
			$table->string('custom_label_1', 255)->nullable();
			$table->string('custom_label_2', 255)->nullable();
			$table->string('custom_label_3', 255)->nullable();
			$table->string('custom_label_4', 255)->nullable();
			$table->string('is_bundle', 5)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feed_rewrite_attributes');
    }
}
