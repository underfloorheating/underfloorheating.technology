<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotes', function (Blueprint $table) {
            $table->increments('id');
			$table->string('first_name');
			$table->string('last_name');
			$table->string('email');
			$table->string('telephone')->nullable();
			$table->string('company_name')->nullable();
			$table->string('vat_number')->nullable();
			$table->string('add1');
			$table->string('add2')->nullable();
			$table->string('town')->nullable();
			$table->string('county')->nullable();
			$table->string('postcode');
			$table->string('country')->nullable();
			$table->text('details');
			$table->string('system_type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotes');
    }
}
