<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sku');
            $table->string('title');
            $table->bigInteger('shop_category')->nullable();
            $table->string('taxonomy_id')->nullable();
            $table->string('parent_id')->nullable();
            $table->string('gtin')->nullable();
            $table->decimal('weight', 4, 2)->default(0);
            $table->string('view_name')->nullable();
            $table->double('price', 8, 2);
            $table->unsignedInteger('stock');
            $table->integer('ebay_id')->nullable();
            $table->boolean('edited')->default(false);
            $table->timestamps();
            $table->tinyInteger('store_id')->default(1)->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
