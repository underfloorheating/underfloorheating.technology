<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuotefilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotefiles', function (Blueprint $table) {
            $table->increments('id');
			$table->string('file_name');
			$table->string('file_ext', 8);
			$table->string('file_type', 50);
			$table->string('upload_name');
			$table->unsignedInteger('quote_id');
            $table->timestamps();

            $table->foreign('quote_id')
            	->references('id')
            	->on('quotes')
            	->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotefiles');
    }
}
