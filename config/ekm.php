<?php

return [

    /*
    |--------------------------------------------------------------------------
    | API Connection details
    |--------------------------------------------------------------------------
    */
    'api' => [
    	'endpoint' => 'https://api.ekm.net/api/v1/',
    	'keys' => [
	        'ufhd' => [
	        	'orderwise' => ENV('EKM_API_UFHD_ORDERWISE'),
	        	'products' => ENV('EKM_API_UFHD_PRODUCTS')
	        ],
	        'ambient' => [
	        	'orderwise' => ENV('EKM_API_AMBIENT_ORDERWISE'),
	        	'products' => ENV('EKM_API_AMBIENT_PRODUCTS')
	        ]
	    ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Number of Days to Query
    |--------------------------------------------------------------------------
    |
    | This represents the number of days ago we should query EKM for orders.
    |
    */
    'number_of_days' => 6,

    /*
    |--------------------------------------------------------------------------
    | Payment type translation
    |--------------------------------------------------------------------------
    |
    | We have to translate the payment types output from the API to match ones
    | held within OrderWise or imports will fail
    |
    */
    'payment_types' => [
    	'PAYPALCHECKOUT' => 'PayPal',
    	'PAYPALEXPRESS' => 'PayPal',
    	'PROTX' => 'SAGEPAYSERVER'
    ]

];