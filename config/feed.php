<?php

return [
	"sku-patterns" => [
		'/^(100|150|200)W[0-9.]+M$/',
		'/^(100|150|200)WCABLE[0-9.]+M?$/',
		'/^(WOOD|CARPET)[0-9.]+M?$/',
		'/^BGT(100|150|200)W[0-9.]+M?$/',
		'/^BGT(CABLE|WOOD)[0-9.]+M?$/',
		'/^DEVI(100|150)W[0-9.]+M$/',
		'/^(2W)?PFM[0-9.]+$/',
		'/^(PP|PW|TH|WV)+(HO|MR|OM|OS|SO|LM|LS|MRC|SOC|WM|WO|WS)+[0-9]+M2?$/',
		'/^(WIS|DWS)\d*$/m'
	]
];
