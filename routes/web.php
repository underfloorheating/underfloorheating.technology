<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*
|-----------------------------------------
| EKM API order collection
|-----------------------------------------
*/
Route::group(['prefix' => 'ekm', 'middleware' => 'ufhs.restricted'], function(){
	Route::get('order/{site}/{id}', 'EkmController@getOrder');
	Route::get('orders/{site}', 'EkmController@getOrders');
	Route::post('confirm-import/{site}', 'EkmController@confirmImport');
	Route::get('out-of-stock/{site}', 'EkmController@getOutOfStockProducts');
});

/*
|-----------------------------------------
| Restricted access to dev environment
|-----------------------------------------
*/
Route::group(['middleware' => 'dev'], function(){

	Route::group(['prefix' => 'gemma'], function(){
		Route::get('labour', 'GemmaController@getLabourCandidates');
		Route::get('thurrock', 'GemmaController@getThurrockCouncillors');
		Route::get('ascl', 'GemmaController@getAsclMembers');
	});
	/*
	|---------------------------
	| Merchant Data Scraping
	|---------------------------
	*/
	Route::group(['prefix' => 'merchants'], function(){
		Route::get('/', 'MerchantsController@index');
		Route::get('/test', 'MerchantsController@testDownload');
		Route::get('/cef', 'MerchantsController@downloadCefData');
		Route::get('/direct/{source}', 'MerchantsController@getDataFromSource');
		Route::get('/db/{source}/{offset?}/{limit?}', 'MerchantsController@downloadMerchantDataUsingPostcodeDatabase');
		Route::get('/{source}/{offset?}/{limit?}/{shortList?}', 'MerchantsController@downloadMerchantData');
	});

	/*
	|------------------
	| General tools
	|------------------
	*/
	Route::group(['prefix' => 'tools'], function(){
		Route::get('get-polypipe-installers', 'ToolsController@downloadPolypipeInstallers');
		Route::get('ekomi-reviews', 'ToolsController@getEkomiReviews');
		Route::get('ekomi-product-reviews', 'ToolsController@getEkomiProductReviews');
		Route::get('ekomi-visitor-reviews', 'ToolsController@getEkomiVisitorReviews');
	});

	/*
	|----------------------------------
	| Ebay publishing functionality
	|----------------------------------
	*/
	Route::group(['prefix' => 'ebay'], function(){
		Route::resource('product', 'ProductController');
		Route::resource('listing', 'ListingController');
		Route::resource('category', 'CategoryController');
	});

	/*
	|------------------------------------
	| Amazon publishing functionality
	|------------------------------------
	*/
	Route::group(['prefix' => 'amazon'], function(){
		Route::resource('product', 'ProductController');
		Route::resource('listing', 'ListingController');
		Route::resource('category', 'CategoryController');
	});
});

/*
|--------------------------------------
| UFHD Request A Quote form handing
|--------------------------------------
*/
Route::group(['prefix' => 'ufhd'], function(){
	Route::get('request-a-quote', 'UfhdController@showQuoteForm');
	Route::post('request-a-quote', 'UfhdController@submitQuote');
	Route::get('download/{quote}/{quoteFile}', 'UfhdController@downloadFile');
});

/*
|--------------------------
| Shopping Feed Handling
|--------------------------
*/
Route::group(['prefix' => 'feed'], function(){
	Route::get('{feed}/refresh', 'FeedController@refreshFeed');
	Route::get('{feed}', 'FeedController@getFeed');
});

/*
|------------------------------------
| Discount My Quote functionality
|------------------------------------
*/
Route::group(['prefix' => 'dmq'], function(){
	Route::get('/', 'DiscountMyQuoteController@index');
	Route::get('email-content/{email}', 'DiscountMyQuoteController@generateQuoteHTMLForEmail');
	Route::get('view/{email?}', 'DiscountMyQuoteController@viewEmail');

	Route::get('refresh', 'DiscountMyQuoteController@requestConfirmationForRefresh');
	Route::post('refresh', 'DiscountMyQuoteController@refreshData');

	Route::get('send', 'DiscountMyQuoteController@requestConfirmationToSendCampaign');
	Route::post('send', 'DiscountMyQuoteController@sendCampaign');

});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
