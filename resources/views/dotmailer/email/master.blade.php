<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office"><head>
<!--[if gte mso 9]><xml><o:OfficeDocumentSettings><o:AllowPNG></o:AllowPNG><o:PixelsPerInch>96</o:PixelsPerInch></o:OfficeDocumentSettings></xml><![endif]-->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>$title</title><style type="text/css" id="editor_required_block">body,html{Margin:0!important;padding:0!important;width:100%!important}
*{-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%}
div[style*="margin: 16px 0"]{margin:0!important}
table,td{mso-table-lspace:0!important;mso-table-rspace:0!important}
table{border-spacing:0!important;border-collapse:collapse!important;table-layout:auto!important}
img{-ms-interpolation-mode:bicubic}
.yshortcuts a{border-bottom:none!important}
.mobile-link--footer a,a[x-apple-data-detectors]{color:inherit!important;text-decoration:underline!important}
.email-width,.row{Margin:0 auto!important}
@media screen and (max-width:504px){
.row .stack-column{display:block!important;width:100%!important;max-width:100%!important;direction:ltr!important;min-width:100%!important}
body .ee-show-on-desktop{display:none!important}
table .ee-hide-on-desktop{display:table;max-height:none;visibility:visible;width:100%!important}
.email-width{width:100vw!important}
}
@media only screen and (min-width:505px){
.row .stack-column{min-width:0!important}
.ee-show-on-desktop{display:block!important}
table.ee-show-on-desktop{display:table!important}
.ee_columns.ee-hide-on-desktop,.ee_element.ee-hide-on-desktop{display:none!important}
}
[owa] .no-stack-column,[owa] .stack-column{Margin:0 -2px;float:none;display:inline-block!important}
.stack-column{min-width:0!important}
body .stack-column{min-width:100%!important}
[owa] .ee-show-on-desktop{display:block!important}
[owa] table.ee-show-on-desktop{display:table!important}
.ee-hide-on-desktop{visibility:hidden;width:0!important;max-height:0;display:block}
body .ee-hide-on-desktop{visibility:visible;width:100%!important;display:table;max-height:none}
#MessageViewBody .ee-show-on-desktop{display:none!important}
a[x-apple-data-detectors]{color:inherit!important;text-decoration:none!important;font-size:inherit!important;font-family:inherit!important;font-weight:inherit!important;line-height:inherit!important}
#MessageViewBody a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit}
u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit}
span.MsoHyperlink,span.MsoHyperlinkFollowed{mso-style-priority:99!important;color:inherit!important;text-decoration:none!important}
@media screen and (max-width:504px){
[owa] .ee-show-on-desktop,body .ee-show-on-desktop{display:none!important}
}</style><style type="text/css" id="editor_required_inline" ee-render="inline">.visually-hidden{display:none;font-size:1px;line-height:1px;max-height:0;max-width:0;opacity:0;overflow:hidden;mso-hide:all;font-family:sans-serif}
.banner-img{border:0;width:100%;max-width:100%;height:auto;vertical-align:bottom}
.background-image-txt{background-position:center center!important;background-size:cover!important}
td.row-container{padding:0;text-align:center;height:100%;vertical-align:top;width:100%}
.stack-col,.stack-column{width:600px;display:inline-block;vertical-align:top}
.no-stack-col,.no-stack-column{display:inline-block;vertical-align:top;float:left}
.stack-column-row{display:inline-block}
.col-font-reset,.font-reset{font-size:14px;text-align:left}
.break-word{display:inline-block;word-wrap:break-word!important;word-break:break-word;word-break:break-all!important}
.text-center{text-align:center}
.margin-0-auto{margin:0 auto}
.border-0{border:0}
.border-collapse{border-collapse:collapse}
.width-100{width:100%}
.margin-auto{Margin:auto}
.mso-height-rule-exactly{mso-height-rule:exactly}
.f-size-0{font-size:0}
.ee_externaldccontent{font-size:initial}
.table-layout-fixed{table-layout:fixed!important}
img{max-width:100%}
ul li{margin:0}
.ee-show-on-desktop{display:none}
.ee_columns.dc-content-hide-on-desktop,.ee_columns.dc-content-hide-on-desktop *,.ee_columns.ee-hide-on-desktop,.ee_columns.ee-hide-on-desktop *,.ee_element.dc-content-hide-on-desktop,.ee_element.dc-content-hide-on-desktop *,.ee_element.ee-hide-on-desktop,.ee_element.ee-hide-on-desktop *{mso-hide:all}
.button-container{text-align:left}
.button-container.button-left{text-align:left}
.button-container.button-left .button-wrapper{float:left;width:initial}
.button-container.button-right{text-align:right}
.button-container.button-right .button-wrapper{float:right;width:initial}
.button-container.button-center{text-align:center}
.button-container.button-center .button-wrapper{float:none;margin:0 auto;width:initial}
.button-container.button-full-width{text-align:center}
.button-container.button-full-width .button-wrapper{width:100%;display:table}
.button-wrapper{text-align:center;display:inline-block}
.button-td{border-radius:3px;background:#222;text-align:center}
.button-td .button-a,.button-td .ee_editable.button-a{margin-bottom:0;text-align:center;line-height:140%;color:#fff}
.button-td .button-a a,.button-td .ee_editable.button-a a{border:0 solid #000;padding:15px 25px;color:inherit;font-family:Arial,sans-serif;font-size:13px;text-align:inherit;text-decoration:none;display:block;border-radius:3px;font-weight:700}
.button-td .button-a a font,.button-td .ee_editable.button-a a font{color:inherit;word-break:break-word;word-wrap:break-word}</style><!--[if mso]>
       <style>
           * {font-family: sans-serif !important;}
           .button-wrapper { padding: 12px 16px 12px 16px; }
       </style>
   <![endif]--><!--[if gte mso 9]>
   <style type="text/css">
       .stack-column{
           width:100%!important;
       }
       .no-stack-column{
           width:100%!important;
       }
   </style>
   <![endif]--><!--[if mso]>
       <style>
       .no-stack-column{
           width:100%!important;
       }
       .stack-column{
          width:100%!important;
       }
       </style>
   <![endif]--><!--[if mso]>
       <style>
       .banner{
           width:900 !important;
       }
       </style>
   <![endif]--><style type="text/css" id="editor_required_emailwidth" ee-render="inline">.email-full-width{width:100%}
.email-body{min-width:100%}
.email-width{max-width:660px}
.row{max-width:660px}
.row.one-cols{max-width:auto}
.row .row{max-width:100%}
.row .row .row-inner{border-width:0;padding:0 0}
.row .row .row-inner.ee_basketcontent,.row .row .row-inner.ee_productcontent{padding:10px}
.ee_dynamic .row-inner.element-pad{padding:0 10px}
.col-inner{border-width:0;padding:0}
.sub-sub-col-inner{border-width:0;padding:0}
.element-pad{padding:10px}
.element-pad.root-element-pad{padding:10px 20px}
.el-pad{padding:10px}
.el-pad.root-el-pad{padding:10px 20px}
.row-inner{border-width:0;padding:0 10px}
.row-inner.ee_basketcontent,.row-inner.ee_productcontent{padding:10px}
.element-bord{border-width:0}
.el-bord{border-width:0}
.one-cols>tbody>tr>td>.no-stack-column,.one-cols>tbody>tr>td>.stack-column{width:100%;min-width:100%;max-width:640px}
.root.one-cols>tbody>tr>td>{width:660px}
.two-cols>tbody>tr>td>.stack-column{max-width:50%;min-width:100%;width:50%}
.two-cols>tbody>tr>td>.no-stack-column{min-width:50%;max-width:50%;width:50%}
.three-cols>tbody>tr>td>.stack-column{max-width:33.333%;min-width:100%;width:33.333%}
.three-cols>tbody>tr>td>.no-stack-column{min-width:33.333%;max-width:33.333%;width:33.333%}
.four-cols>tbody>tr>td>.stack-column{max-width:25%;min-width:100%;width:25%}
.four-cols>tbody>tr>td>.no-stack-column{min-width:25%;max-width:25%;width:25%}
.five-cols>tbody>tr>td>.stack-column{max-width:20%;min-width:100%;width:20%}
.five-cols>tbody>tr>td>.no-stack-column{min-width:20%;max-width:20%;width:20%}
.six-cols>tbody>tr>td>.stack-column{max-width:16.667%;min-width:100%;width:16.667%}
.six-cols>tbody>tr>td>.no-stack-column{min-width:16.667%;max-width:16.667%;width:16.667%}
.seven-cols>tbody>tr>td>.stack-column{max-width:14.286%;min-width:100%;width:14.286%}
.seven-cols>tbody>tr>td>.no-stack-column{min-width:14.286%;max-width:14.286%;width:14.286%}
.eight-cols>tbody>tr>td>.stack-column{max-width:12.5%;min-width:100%;width:12.5%}
.eight-cols>tbody>tr>td>.no-stack-column{min-width:12.5%;max-width:12.5%;width:12.5%}
.banner-padding{padding:0 0}
.hero-image-padding{padding:0 0}
.background-image-txt-mso{width:680px;height:175px;background-position:center center!important}
.bg-img-txt-width{max-width:500px}
.bg-img-txt-text{padding:0 0}
.col-font-reset,.ee_element,.font-reset{min-width:100%}
.button-wrapper{min-width:auto}</style><style ee-render="block">
.button-td:hover .button-a { transition: all 100ms ease-in; background: rgba(255, 255, 255, 0.2) !important; }
</style><style type="text/css" ee-render="inline" ee-base="">
.ExternalClass p { MARGIN: 0px; }
.ee_editable p, .ee_editable li, .ee_editable td, .ee_editable a, .ee_editable font { word-wrap: break-word; word-break: break-word; }
.ee_element .button-container.button-left { text-align: left; }
.ee_element .button-container.button-left .button-wrapper { float: left; }
.ee_element .button-container.button-right { text-align: right; }
.ee_element .button-container.button-right .button-wrapper { float: right; }
.ee_element .button-container.button-center { text-align: center; }
.ee_element .button-container.button-center .button-wrapper { float: none; margin: 0 auto; }
.ee_element .button-container.button-full-width { text-align: center; }
.ee_element .button-container.button-full-width .button-wrapper { width: 100%; display: table; }
.button-wrapper { text-align: center; display: inline-block; }
.button-td .button-a, .button-td .ee_editable.button-a {color: #fff; margin-bottom: 0; }
.button-td .button-a a, .button-td .ee_editable.button-a a {color:inherit; text-align: inherit; display: block; height: 100%; }
.button-td .button-a a font, .button-td .ee_editable.button-a a font {color:inherit; word-break: break-word; word-wrap: break-word; }
-->
</style><style ee-styles="" ee-render="inline">
/* Auto-generated style block.  Only edit if you know what you are doing! */
.base-styles { color: #333333; font-family: Arial, sans-serif; font-size: 14px; line-height: 22px; Margin: 0px; }
body, td { font-family: Arial, sans-serif; }
.ee_editable, .ee_editable td { color: #333333; font-size: 14px; line-height: 22px; }
.ee_editable p { Margin: 0px; }
.ee_editable .h1, .h1.ee_editable { font-size: 24px; font-weight: bold; color: #333333; line-height: 34px; }
.ee_editable .h2, .h2.ee_editable { font-size: 22px; font-weight: normal; color: #333333; line-height: 31px; }
.ee_editable .h3, .h3.ee_editable { font-size: 18px; font-weight: normal; color: #333333; line-height: 29px; }
.ee_editable .h4, .h4.ee_editable { font-size: 16px; font-weight: bold; }
.ee_editable p.disclaimer { font-size: 14px; color: #3a3a3a; line-height: 22px; text-align: left; }
.button-td.Default.definition { color: #ffffff; font-family: Arial, sans-serif; font-size: 14px; font-style: normal; font-weight: bold; line-height: 20px; text-align: center; border: 0px solid #000000; border-radius: 3px; padding: 8px 15px; align-items: flex-start; background: #333333; text-decoration: none; }
.button-container { text-align: left; }
.button-container .button-wrapper { float: left; }
.button-container  .button-td { border-radius: 3px; background: #333333; }
.button-container  .button-td .button-a, .button-container .ee_editable.button-a { color: #ffffff; line-height: 140%; text-align: center; }
.button-container  .button-td .button-a a, .button-container .ee_editable.button-a a { color: #ffffff; font-family: Arial, sans-serif; font-size: 14px; font-style: normal; font-weight: bold; line-height: 140%; text-align: center; border: 0px solid #000000; border-radius: 3px; padding: 8px 15px; text-decoration: none; }
</style></head>
<body><!-- Easy Editor -->
<table cellpadding="0" cellspacing="0" border="0" height="100%" width="100%" class="border-collapse ee_responsive_campaign" style="table-layout: auto;" ee-show-font-styles="" ee-template-version="8.4" ee-mobile-first="true">
  <tbody>
    <tr>
      <td valign="top">
        <center class="width-100">
          <table cellspacing="0" cellpadding="0" border="0" align="center" width="100%" class="email-body" style="background-color: #ffffff;table-layout: auto;">
            <tbody>
              <tr>
                <td class="email-full-width margin-0-auto">
                  <!--[if (gte mso 9)|(IE)]><table cellspacing="0" cellpadding="0" border="0" style="width:660px;" width="660" align="center"><tr><td><![endif]-->
                  <table cellspacing="0" cellpadding="0" border="0" align="center" width="100%" class="email-width" style="table-layout: auto;">
                    <tbody>
                      <tr>
                        <td>
                          <table cellpadding="0" cellspacing="0" border="0" valign="top" width="100%" style="table-layout: auto;">
                            <tbody>
                              <tr>
                                <td class="row-container">

                                  @yield('content')

                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                </td>
              </tr>
            </tbody>
          </table>
        </center>
      </td>
    </tr>
  </tbody>
</table></body></html>