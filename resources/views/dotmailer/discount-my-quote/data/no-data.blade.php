@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            	@include('dotmailer.discount-my-quote.partials.card-header', ['heading' => 'There is no data?'])

                <div class="card-body">
					<p>There are no contacts in the Discount My Quote address book within Dotmailer.  <a href="/dmq/refresh">Refresh the data</a> before continuing.</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection