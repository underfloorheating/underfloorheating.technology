@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            	@include('dotmailer.discount-my-quote.partials.card-header', ['heading' => 'The data has been refreshed'])

                <div class="card-body">
                	<p>The data from <a href="https://docs.google.com/spreadsheets/d/1kCUe3Od6Or7tjIkmelxri6veogfmZG5sO5L-m8_lwHQ/edit#gid=0" target="_blank">this Google Sheet</a> has now been copied to the local database and pushed into the <a href="https://r1-app.dotdigital.com/Contacts/ListContacts.aspx?i=15099331&from=%2FContacts%2FDefault.aspx%3Fp%3D1%26s%3DName%20asc%26" target="_blank">Discount My Quote address book</a> in DotMailer</p>
                    <p>Ready to <a href="/dmq/send">send the campaign</a>?</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection