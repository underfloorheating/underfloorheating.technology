@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            	@include('dotmailer.discount-my-quote.partials.card-header', ['heading' => 'Refresh the <em>Discount My Quote</em> data?'])

                <div class="card-body">
                    <p>If you would like to refresh the Discount My Quote data, click the button below.  This will wipe all current data in the local DB and update the table with the data found in <a href="https://docs.google.com/spreadsheets/d/1kCUe3Od6Or7tjIkmelxri6veogfmZG5sO5L-m8_lwHQ/edit#gid=0" target="_blank">this Google Sheet</a>.</p>
                    <p>This data will then be pushed to the <a href="https://r1-app.dotdigital.com/Contacts/ListContacts.aspx?i=15099331&from=%2FContacts%2FDefault.aspx%3Fp%3D1%26s%3DName%20asc%26" target="_blank">Discount My Quote address book</a> in DotMailer</p>

                    <form action="/dmq/refresh" method="POST">
                    	@csrf
						<button type="submit" class="btn btn-primary">Refresh the data ></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection