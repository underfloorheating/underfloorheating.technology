@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            	@include('dotmailer.discount-my-quote.partials.card-header', ['heading' => 'The data in Dotmailer is old'])

                <div class="card-body">
                	<p>It looks like the data in Dotmailer is left over from the last time the Discount My Quote campaign was run.</p>
                	<p><a href="/dmq/refresh">Refresh the data</a> before continuing.</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection