@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card mb-3">
            	@include('dotmailer.discount-my-quote.partials.card-header', ['heading' => 'Use specific customer'])
                <div class="card-body">
                    <p>If you would like to view the email for a specific client, enter their email address here</p>
					<div class="form-group">
						<input type="text" name="email" id="email" class="form-control" placeholder="enter email address" />
					</div>
					<button type="submit" class="btn btn-primary" id="button">View email</button>
                </div>
            </div>
            <div class="card">
            	@include('dotmailer.discount-my-quote.partials.card-header', ['heading' => 'Email content'])
                <div class="card-body">
                    {!! $content !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
	$(document).ready(function(){
		$('#button').on('click',function(event){
			event.preventDefault();
			if($('#email').val() == '' || /@+/.test($('#email').val()) == false) {
				$('#email').val("");
				return false;
			}
			location.href = '/dmq/view/' + $('#email').val();
		});
	});
</script>
@endsection