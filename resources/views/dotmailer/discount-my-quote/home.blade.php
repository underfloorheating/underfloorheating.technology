@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Discount My Quote Manager</div>

                <div class="card-body">
                    <p>What would you like to do?</p>
					<ul>
						<li><a href="/dmq/refresh">Refresh the data</a></li>
						<li><a href="/dmq/send">Send the campaign</a></li>
						<li><a href="/dmq/view">View the email</a></li>
					</ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection