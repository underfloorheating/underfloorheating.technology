@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            	@include('dotmailer.discount-my-quote.partials.card-header', ['heading' => 'Send the <em>Discount My Quote</em> campaign?'])

                <div class="card-body">
                    <p>If you would like to send the <em>Discount My Quote</em> campaign, click the button below.  This will send the campaign to the <strong>{{ $numContacts }}</strong> contacts in the <em>Discount My Quote</em> address book</p>

                    <form action="/dmq/send" method="POST">
                    	@csrf
						<button type="submit" class="btn btn-primary">Send the campaign ></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection