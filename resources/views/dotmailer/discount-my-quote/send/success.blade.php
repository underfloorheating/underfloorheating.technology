@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            	@include('dotmailer.discount-my-quote.partials.card-header', ['heading' => 'Send was successful'])

                <div class="card-body">
                    <p>The campaign was created successfully</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection