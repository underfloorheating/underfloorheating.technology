<!--[if mso]><table border="0" cellspacing="0" cellpadding="0" align="center" style="width:660px;" width="660"><tr><td align="center" valign="top" style="width:660px;" width="660"><![endif]-->

<table cellpadding="0" cellspacing="0" class="col-font-reset ee_element ee_textelement" width="100%" ee-type="element" data-title="Text" style="table-layout: auto;">
	<tbody>
		<tr>
			<td valign="top" align="left" class="element-pad element-bord root-element-pad" style="padding-top: 10px;padding-right: 10px;padding-bottom: 10px;padding-left: 10px;">
				<div class="ee_editable">
					<p>Hi,</p>
					<p>I can see from our records, you currently have {{ $quoteCount }} outstanding underfloor heating quote{{ $quoteCount > 1 ? 's' : '' }} with us. How {{ $quoteCount > 1 ? 'are these projects' : 'is this project' }} coming along? Is there anything I can do to help? Any questions I can answer for you? If so feel free to give me a call, my number is below, I'll be glad to help in any way I can!</p>

					@if(count($quotes['maximum_discount_applied']) > 0)
						<p>Here are your outstanding quote details</p>
						<br />
						<table class="quote-values" cellspacing="0" cellpadding="6" border="0" style="border-collaspe: collapse;border-spacing: 0;font-family: arial, sans-serif;font-size: 12px;">
							<thead>
								<tr>
									<th bgcolor="#BBBBBB" valign="middle" align="center" style="padding: 6px 10px;border-left: 1px solid #DEDEDE;border-bottom: 1px solid #DEDEDE;background-color: #BBBBBB;font-weight: bold;">Quote Number</th>
									<th bgcolor="#BBBBBB" valign="middle" align="center" style="padding: 6px 10px;border-left: 1px solid #DEDEDE;border-bottom: 1px solid #DEDEDE;background-color: #BBBBBB;font-weight: bold;">Quote Ref</th>
									<th bgcolor="#BBBBBB" valign="middle" align="center" style="padding: 6px 10px;border-left: 1px solid #DEDEDE;border-bottom: 1px solid #DEDEDE;background-color: #BBBBBB;font-weight: bold;">Quote Date</th>
									<th bgcolor="#BBBBBB" valign="middle" align="center" style="padding: 6px 10px;border-left: 1px solid #DEDEDE;border-bottom: 1px solid #DEDEDE;border-right: 1px solid #DEDEDE;background-color: #BBBBBB;font-weight: bold;">Quote Net</th>
								</tr>
							</thead>
							<tbody>
								@foreach($quotes['maximum_discount_applied'] as $quote)
								<tr>
									<td align="left" valign="middle" style="padding: 6px 10px;border-left: 1px solid #DEDEDE;border-bottom: 1px solid #DEDEDE;">{{ $quote->order_number }}</td>
									<td align="left" valign="middle" style="padding: 6px 10px;border-left: 1px solid #DEDEDE;border-bottom: 1px solid #DEDEDE;">{{ $quote->customer_order_ref }}</td>
									<td align="left" valign="middle" style="padding: 6px 10px;border-left: 1px solid #DEDEDE;border-bottom: 1px solid #DEDEDE;">{{ date('d/m/Y', strtotime($quote->order_date)) }}</td>
									<td align="left" valign="middle" style="padding: 6px 10px;border-left: 1px solid #DEDEDE;border-bottom: 1px solid #DEDEDE;border-right: 1px solid #DEDEDE;">£{{ number_format($quote->order_net) }}</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					@endif

					@if(count($quotes['discountable']) > 0)
						<br />
						<p>I am happy to be able to offer you a discount on {{ count($quotes['discountable']) }} of your quotes in addition to any existing discount you have already been given.  Check out the table below to see how much you can save by calling me today!</p>
						<br />
						<table class="quote-values" cellspacing="0" cellpadding="6" border="0" style="border-collaspe: collapse;border-spacing: 0;font-family: arial, sans-serif;font-size: 12px;">
							<thead>
								<tr>
									<th bgcolor="#BBBBBB" valign="middle" align="center" style="padding: 6px 10px;border-left: 1px solid #DEDEDE;border-bottom: 1px solid #DEDEDE;background-color: #BBBBBB;font-weight: bold;">Quote Number</th>
									<th bgcolor="#BBBBBB" valign="middle" align="center" style="padding: 6px 10px;border-left: 1px solid #DEDEDE;border-bottom: 1px solid #DEDEDE;background-color: #BBBBBB;font-weight: bold;">Quote Ref</th>
									<th bgcolor="#BBBBBB" valign="middle" align="center" style="padding: 6px 10px;border-left: 1px solid #DEDEDE;border-bottom: 1px solid #DEDEDE;background-color: #BBBBBB;font-weight: bold;">Quote Date</th>
									<th bgcolor="#BBBBBB" valign="middle" align="center" style="padding: 6px 10px;border-left: 1px solid #DEDEDE;border-bottom: 1px solid #DEDEDE;background-color: #BBBBBB;font-weight: bold;">Quote Net</th>
									<th bgcolor="#BBBBBB" valign="middle" align="center" style="padding: 6px 10px;border-left: 1px solid #DEDEDE;border-bottom: 1px solid #DEDEDE;background-color: #BBBBBB;font-weight: bold;">Discount %</th>
									<th bgcolor="#BBBBBB" valign="middle" align="center" style="padding: 6px 10px;border-left: 1px solid #DEDEDE;border-bottom: 1px solid #DEDEDE;border-right: 1px solid #DEDEDE;background-color: #fb5858;color:#FFFFFF;font-weight: bold;">You Could Save</th>
								</tr>
							</thead>
							<tbody>
								@foreach($quotes['discountable'] as $quote)
								<tr>
									<td align="left" valign="middle" style="padding: 6px 10px;border-left: 1px solid #DEDEDE;border-bottom: 1px solid #DEDEDE;">{{ $quote->order_number }}</td>
									<td align="left" valign="middle" style="padding: 6px 10px;border-left: 1px solid #DEDEDE;border-bottom: 1px solid #DEDEDE;">{{ $quote->customer_order_ref }}</td>
									<td align="left" valign="middle" style="padding: 6px 10px;border-left: 1px solid #DEDEDE;border-bottom: 1px solid #DEDEDE;">{{ date('d/m/Y', strtotime($quote->order_date)) }}</td>
									<td align="left" valign="middle" style="padding: 6px 10px;border-left: 1px solid #DEDEDE;border-bottom: 1px solid #DEDEDE;">£{{ number_format($quote->order_net) }}</td>
									<td align="left" valign="middle" style="padding: 6px 10px;border-left: 1px solid #DEDEDE;border-bottom: 1px solid #DEDEDE">{{ $quote->available_discount * 100 }}%</td>
									<td align="left" valign="middle" style="padding: 6px 10px;border-left: 1px solid #DEDEDE;border-bottom: 1px solid #DEDEDE;border-right: 1px solid #DEDEDE;">£{{ number_format($quote->order_net * $quote->available_discount, 2) }}</td>
								</tr>
								@endforeach
							</tbody>
						</table>
						<br />
						<p>Simply use the following code when you call to make {{ count($quotes['discountable']) > 1 ? 'these extra savings' : 'this extra saving' }}!</p>
						<p style="line-height: 22px;"><b><font style="font-size: 20px;">DISCOUNTMYQUOTE</font></b></p>
						<p>This is a limited offer and will expire on 31st August 2019, so give me a call before it's too late! Terms and conditions apply *</p>
						<p>Note: the code above cannot be used on our website as the quote we provided was bespoke to your project.</p>
					@else
						<br />
						<p>I look forward to hearing from you soon.</p>
						<br />
					@endif
				</div>
			</td>
		</tr>
	</tbody>
</table>
<!--[if mso]></table><![endif]-->