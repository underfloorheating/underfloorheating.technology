<!DOCTYPE html>
<html>
<head>
	<title></title>

	<style>
		body {
			font: 13px/17px Arial;
		}
		.thumbnail {
			float: right;
			width: 60px;
			height: auto;
			margin: 0 0 20px 20px;
		}
		table {
			width: 100%;
			border-collapse: collapse;
		}
		th, td {
			padding: 4px 10px;
		}
		th {
			border: 1px solid #ccc;
			background: #eee;
		}
		td {
			border: 1px solid #eee;
		}
	</style>
</head>
<body>

	@foreach($items as $itemId => $item)
		<img src="{{ $item['image'] }}" class="thumbnail">
		<h3>{{ $item['title'] }} - <a href="{{ $item['url'] }}">{{ $itemId }}</a></h3>
		<p>{{ $item['primaryCategory']['name'] }}</p>
		<table>
			<tr>
				<th>ID</th>
				<th>Title</th>
				@foreach($item['variants'][0]['choices'] as $name => $value)
					<th>{{ $name }}</th>
				@endforeach
				<th>Price</th>
			</tr>
			@foreach($item['variants'] as $var)
				<tr>
					<td>{{ $itemId }}</td>
					<td>{{ $item['title'] }}</td>
					@foreach($var['choices'] as $val)
						<td>{{ $val }}</td>
					@endforeach
					<td>{{ $var['price'] }}</td>
				</tr>
			@endforeach
		</table>
	@endforeach
</body>
</html>
