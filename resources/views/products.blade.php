@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row justify-content-center">
		<div class="col-xs-12">
			<div class="card">
				<div class="card-header">Products</div>

				<div class="card-body">
					<table class="table">
						<thead>
							<tr>
								<th>Title</th>
								<th>SKU</th>
								<th>Weight</th>
								<th>Price</th>
							</tr>
						</thead>
						<tbody>
							@foreach($products as $product)
							<tr>
								<th>{{ $product->title }}</th>
								<th>{{ $product->sku }}</th>
								<th>{{ $product->weight }}</th>
								<th>{{ $product->price }}</th>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
