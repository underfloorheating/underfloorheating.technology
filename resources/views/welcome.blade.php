<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<script>window.Laravel = { csrfToken: '{{ csrf_token() }}' }</script>
	<title>Underfloor Heating Technology</title>

	<style>
		html,
		body {
			height: 100%;
		}
		.flex-container {
			display: flex;
			align-items: center;
			justify-content: center;
			height: 100%;
		}
		h1 {
			font-family: Arial, sans-serif;
			color: #36454f;
		}
	</style>
</head>
<body>
	<div class="flex-container">
		<div class="flex-item">
			<h1>Underfloor Heating Technology</h1>
		</div>
	</div>
</body>
</html>
