@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Merchant download response</div>

                <div class="card-body">
                	@if(isset($responses['new']))
	                    <h2>{{ count($responses['new']) }} New records added</h2>
                    @endif
                	@if(isset($responses['exist']))
	                    <h2>{{ count($responses['exist']) }} Records already existed</h2>
                    @endif
                    <h2>Postcodes used</h2>
                    @foreach($regions as $region)
						<span>{{ $region['code'] }}, </span>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection