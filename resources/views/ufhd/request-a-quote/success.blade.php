@extends('ufhd.layout')

@section('content')
	<div class="fluid-container">
		<h4>Success!</h4>
		<p>Your quote request has been submitted successfully and is winging it's way to our highly experienced quote team.</p>
		<p>One of our installation experts will be in contact shortly to discuss your project further and provide you with a detailed quote.</p>
		<p>If you do have any questions in the mean time, please don't hesitate to give our team a ring on 0800 141 2643 or get in touch through our <a href="https://underfloorheating-direct.com/contact-us-233-w.asp" target="_parent">contact form</a>.</p>
	</div>
@endsection