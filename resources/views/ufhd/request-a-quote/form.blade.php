@extends('ufhd.layout')

@section('content')

<form action="/ufhd/request-a-quote" method="POST" id="quote-form" enctype="multipart/form-data" class="needs-validation" novalidate>
	@csrf
	<div class="fluid-container">
		<section class="row">
			<div class="col-12">
				<p>If your working on a project and would like a quote, simply complete the form below. We have provided the facility to send us your architectural drawings, you can scan your plans and upload as many files as you need at the bottom of the form below.</p>
				<p><strong>If you have any questions or require any help, please feel free to contact our sales team on <span class="red">0800 141 2643</span></strong></p>
			</div>
		</section>
		<section class="row">
			<div class="col-12 col-sm-6">
				<h4>Your Details</h4>
				<div class="form-group">
					<label for="first-name">First Name<span>*</span></label>
					<input type="text" name="first_name" id="first-name" value="{{ old('first_name') }}" maxlength="200" class="form-control {{ $errors->has('first_name') ? 'isinvalid' : (old('first_name') ? 'isvalid' : '') }}" required />
					<div class="valid-feedback">
						Looks good!
					</div>
					<div class="invalid-feedback">
						Please enter your first name.
					</div>
				</div>
				<div class="form-group">
					<label for="last-name">Last Name<span>*</span></label>
					<input type="text" name="last_name" id="last-name" value="{{ old('last_name') }}" maxlength="30" class="form-control {{ $errors->has('last_name') ? 'isinvalid' : (old('last_name') ? 'isvalid' : '') }}" required />
					<div class="valid-feedback">
						Looks good!
					</div>
					<div class="invalid-feedback">
						Please enter your last name.
					</div>
				</div>
				<div class="form-group">
					<label for="email">Email<span>*</span></label>
					<input type="email" name="email" id="email" value="{{ old('email') }}" maxlength="45" class="form-control {{ $errors->has('email') ? 'isinvalid' : (old('email') ? 'isvalid' : '') }}" required />
					<div class="valid-feedback">
						Looks good!
					</div>
					<div class="invalid-feedback">
						Please enter your email address.
					</div>
				</div>
				<div class="form-group">
					<label for="telephone-number">Telephone</label>
					<input type="tel" name="telephone" id="telephone-number" value="{{ old('telephone') }}" maxlength="18" class="form-control" />
				</div>
			</div>
			<div class="col-12 col-sm-6">
				<h4>Billing Address</h4>
				<div class="form-group">
					<label for="add1">House number / name<span>*</span></label>
					<input type="text" name="add1" id="add1" value="{{ old('add1') }}" maxlength="255" class="form-control {{ $errors->has('add1') ? 'isinvalid' : (old('add1') ? 'isvalid' : '') }}" required />
					<div class="valid-feedback">
						Looks good!
					</div>
					<div class="invalid-feedback">
						Please enter the name or number of your house.
					</div>
				</div>
				<div class="form-group">
					<label for="add2">Street</label>
					<input type="text" name="add2" id="add2" value="{{ old('add2') }}" maxlength="255" class="form-control" />
				</div>
				<div class="form-group">
					<label for="town">Town</label>
					<input type="text" name="town" id="town" value="{{ old('town') }}" maxlength="100" class="form-control" />
				</div>
				<div class="form-group">
					<label for="county">County</label>
					<input type="text" name="county" id="county" value="{{ old('county') }}" maxlength="60" class="form-control" />
				</div>
				<div class="form-group">
					<label for="postcode">Postcode<span>*</span></label>
					<input type="text" name="postcode" id="postcode" value="{{ old('postcode') }}" maxlength="14" class="form-control {{ $errors->has('postcode') ? 'isinvalid' : (old('postcode') ? 'isvalid' : '') }}" required />
					<div class="valid-feedback">
						Looks good!
					</div>
					<div class="invalid-feedback">
						Please enter your postcode.
					</div>
				</div>
			</div>
		</section>
		<section class="row">
			<div class="col-12">
				<h4>Company Details (if applicable)</h4>
			</div>
			<div class="col-12 col-sm-6">
				<div class="form-group">
					<label for="company-name">Company Name</label>
					<input type="text" name="company_name" id="company-name" value="{{ old('company_name') }}" maxlength="255" class="form-control" />
				</div>
			</div>
			<div class="col-12 col-sm-6">
				<div class="form-group">
					<label for="vat-number">VAT No.</label>
					<input type="text" name="vat_number" id="vat-number" value="{{ old('vat_number') }}" maxlength="20" class="form-control" />
				</div>
			</div>
		</section>
		<section class="row">
			<div class="col-12 col-sm-6">
				<h4>Project Details</h4>
				<div class="form-group">
					<label for="details">Overview of project requirements<span>*</span></label>
					<textarea name="details" id="details" class="form-control {{ $errors->has('details') ? 'isinvalid' : (old('details') ? 'isvalid' : '') }}" placeholder="Please provide us with as much information as possible i.e. subfloor type, rooms sizes etc." required>{{ old('details') }}</textarea>
					<div class="valid-feedback">
						Looks good!
					</div>
					<div class="invalid-feedback">
						Please provide as much detail about your project as you can.  The more detail you can supply the quicker we will be able to provide you with a quote.
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-6">
				<h4>Project Type</h4>
				<div class="form-check">
					<input type="radio" id="system-electric" name="system_type" value="electric" class="form-check-input" required/>
					<label for="system-electric" class="form-check-label">Electric Underfloor Heating</label>
				</div>
				<div class="form-check">
					<input type="radio" id="system-water" name="system_type" value="water" class="form-check-input" required/>
					<label for="system-water" class="form-check-label">Water Underfloor Heating</label>
				</div>
				<div class="form-check">
					<input type="radio" id="system-unknown" name="system_type" value="unsure" class="form-check-input" required/>
					<label for="system-unknown" class="form-check-label">I'm not sure?</label>
					<div class="valid-feedback">
						Looks good!
					</div>
					<div class="invalid-feedback">
						Please select which type of project this is.
					</div>
				</div>
				<div id="files">
					<div class="form-group">
						<label class="custom-file-input">Choose file</label>
						<input type="file" name="file-1" class="form-control-file" id="file-1" aria-describedby="file-help-block"/>
					</div>
				</div>
				<div class="form-group">
					<a href="#" id="add-file" rel="1">Add another file +</a>
					<small id="file-help-block" class="form-text text-muted">Files must be no larger than 10Mb and can only be image files, PDF documents or Microsoft Office files.</small>
				</div>
				<div class="form-group">
					<button type="submit"class="btn btn-primary" >submit request</button>
				</div>
			</div>
		</section>
	</div>
</form>

@endsection

@section('scripts')

<script>
	(function() {
		'use strict';
		window.addEventListener('load', function() {
			var forms = document.getElementsByClassName('needs-validation');
			var validation = Array.prototype.filter.call(forms, function(form) {
				form.addEventListener('submit', function(event) {
					if (form.checkValidity() === false) {
						event.preventDefault();
						event.stopPropagation();
					}
					form.classList.add('was-validated');
				}, false);
			});
		}, false);
	})();

	$(document).ready(function () {
		$('#add-file').on('click', function (event) {
			event.preventDefault();
			var num = parseInt($(this).attr('rel'));
			num++;
			$(this).attr('rel', num);
			$('#files').append(
				$('<div class="form-group"><input type="file" name="file-'+num+'" class="form-control-file" id="file-'+num+'" aria-describedby="file-help-block"/></div>')
				);
		});

		$('input[type=file]').change(function () {
			var file = $(this)[0].files[0];
			if (file.size > 2000000) {
				alert('The file you are trying to upload is too big.  The maximum size is 2Mb per file.');
				$(this).val('');
			}
		});
	});
</script>

@endsection