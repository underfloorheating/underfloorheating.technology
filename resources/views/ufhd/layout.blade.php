<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Underfloor Heating Direct</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="stylesheet" media="screen" href="{{ asset('ufhd/css/style.css') }}">
    </head>

    <body>
		@yield('content')

		<script src="{{ asset('ufhd/js/vendor.js') }}"></script>
		<script src="{{ asset('ufhd/js/app.js') }}"></script>
	    @yield('scripts')

    </body>
</html>