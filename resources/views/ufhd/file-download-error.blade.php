@extends('ufhd.layout')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-12 text-center">
			<h1>Oops, something has gone wrong</h1>
			<p>The file you are trying to download doesn't seem to exist in the file system.</p>
			<p>Please contact Chris Sewell using <a href="mailto:chris@theunderfloorheatingstore.com">chris@theunderfloorheatingstore.com</a> to report the issue.</p>
		</div>
	</div>
</div>
@endsection